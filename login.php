<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/config/session.class.php';
include 'includes/class/config/system.class.php';
include 'includes/class/config/plantilla.class.php';

//-----------------------------------------------------------------//
//--- Bloque de validación... 
//-----------------------------------------------------------------//
session_start();
if( isset($_POST['usr']) && isset($_POST['psw']) ){
    if( !empty($_POST['usr']) && !empty($_POST['psw']) ){        
        $session = new Session($_POST['usr'], $_POST['psw']);
        if( $session->getResult() > 0 ){
            $_SESSION['admitted_xsisp'] = true;
            $_SESSION['xlogin_id_sisp'] = $session->GetResult();
            //--- Registro del log...
            $oSys = new System();
            $oSys->registroLog($_SESSION['xlogin_id_sisp'], '--', '--', 'Acceso');
            //--- Se genera los parámetros necesarios para calcular la ruta relativa...
            /*
            * 1 : Si el sistema se hospeda sobre la carpeta raiz.
            * 2 : Si el sistema se hospeda en una subcarpeta.
            */
            $_SESSION['xNumLevelRoot'] = 2;
            header('Location: ' . $oSys->getServer() . 'index.php?m=0');
        }
        else{
            $_SESSION['intentos'] = ( isset($_SESSION['intentos']) )  ? $_SESSION['intentos']+1 : 1;
            if( $_SESSION['intentos'] < 4 )
                $html_error = '<p class="pMsjError" style="margin-bottom: 10px;"><span class="spnIconError"></span>Error: el nombre de usuario y/o contraseña son incorrectos</p>';
            else{
                //-- Se bloque la IP del cliente...
                $html_error = '<p class="pMsjError" style="margin-bottom: 10px;"><span class="spnIconError"></span>¡Ha excedido el número de intentos para accesar al sistema!</p>';
            }
        }
    }
    else
        $html_error = '<p class="pMsjError" style="margin-bottom: 10px;"><span class="spnIconError"></span>Error: debe complementar los datos para accesar...</p>';
}
else
    $html_error = '';

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo'    => 'SISP :: Login',
                'usr'       => 0,
                'scripts'   => array('<script type="text/javascript" src="includes/js/xlogin.js"></script>'),
                'header'    => true,
                'menu'      => false,
                'menuLogin' => true);
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
<div id="xLogin" style="width: 650px; height: auto; position: relative; margin: auto auto; margin-top: 10%;">
    <?php echo $html_error;?>
    <?php
    if( !isset($_SESSION['intentoss']) ||  $_SESSION['intentoss'] < 4 ){
    ?>
    <div id="dvContentLogin" class="dvContentLogin">
        <div class="dvLogin-Left">
            <div style="height: 130px; margin: auto auto; padding-top: 10px; position: relative; text-align: center; width: 280px;">
                <p style="color: #ffffff; font-family: arial, sans-serif; font-size: 14pt; font-weight: bold;">
                    <?php echo APP_DESCRIPTION; ?>
                </p>
                <img src="includes/css/imgs/login/login_mngr.png" alt="" style="margin: auto auto;" />
            </div>
        </div>
        <div class="dvLogin-Center">
            <div class="dvLogin-Form">
                <form name="form-login" id="Form-Login" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="application/x-www-form-urlencoded" autocomplete="off">
                    <label for="txtUsr" class="dvLogin-Form-Label">Usuario:</label>
                    <input type="text" name="usr" id="txtUsr" value="<?php if( isset($_POST['usr']) ) echo $_POST['usr'];?>" maxlength="30" autofocus="true" placeholder="Intruduzca su nombre de usuario" required="required" class="input-Text" style="padding: 7px 3px 7px 3px; text-align: center; width: 245px;" />
                    <label for="txtPsw" class="dvLogin-Form-Label">Contraseña:</label>
                    <input type="password" name="psw" id="txtPsw" maxlength="30" placeholder="Intruduzca su contraseña" required="required" class="input-Text" style="padding: 7px 3px 7px 3px; text-align: center; width: 245px;" />
                    <p class="dvLogin-Form-pButton">
                       <button type="submit" name="btnValidar" class="dvLogin-Form-Button">Accesar</button>
                    </p>
                </form>
            </div>
        </div>        
    </div>
    <div class="dvLogin-Footer"> Powered by: <a style="color:#fff; text-decoration: none;" href="<?php echo APP_AUTHORSITE;?>" title="<?php echo APP_AUTHORSITE;?>" target="_blank"><?php echo APP_POWEREDBY;?></a></div>
    <?php
    }
    ?>
</div>
<?php
$plantilla->PaginaFin();
?>