Las carpetas estar�n estructuradas de la siguiente manera:

	- adm : Bloque administrativo


	- log : Bloque log�stico


	- ope : Bloque operativo


	- includes: Todos los archivos generales para el funcionamiento del sistema
		-> class: Todas las clases .php (tablas, configuraci�n, etc.)
		-> css: Todas las hojas de estilo generales
		-> js: Todas las librer�as .js (jquery, jquery.ui, validate, etc.)
		-> lib: Todas las librer�as adicionales php (fpdf, thumbnails, etc)



	- pry_ : Todas las carpetas que s�lo estar�n disponibles durante el desarrollo del proyecto,
		 y no se incluir�n en la distribuci�n del sistema.