$(document).ready(function(){ 
    // Barra de opciones flotante
    var barraOp = $('#dvBarraOpciones');
    var topBar = $(window).height() - (barraOp.height() + 7);
    var leftBar = (($(window).width() / 2) - (barraOp.width() / 2));
    barraOp.css({
        top: topBar,
        left: leftBar,
    });
    $(window).resize(function(){
        var topBar = $(this).height() - 60;
        var leftBar = (($(this).width() / 2) - (barraOp.width() / 2));
        barraOp.css({
            top: topBar,
            left: leftBar,
        });
    });
    
	//codigo para la fecha de siniestro
	$('#txtFechaIni').datepicker({
        yearRange: '2010:2030',
    });
	//codigo para la fecha de siniestro
	$('#txtFechaFin').datepicker({
        yearRange: '2010:2030',
    });
       
     //codigo para validar campos
    $('#frmRegistro').validate({
        rules:{
            // Datos Personales
            txtFechaIni: 'required',
            txtFechaFin: 'required',
            cbxTipoMtto:{
                required: true,
                min: 1,
            },
            cbxTipoServicio:{
                required: true,
                min: 1,
            },
            cbxEntidad:{
                required: true,
                min: 1,
            },
            cbxMunicipio:{
                required: true,
                min: 1,
            },
            txtKmTotal:{    
                required: true,
                digits: true,
                min: 1,
                max: 999999,
            },
    	},
    	messages:{
            txtFechaIni: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaFin: '<span class="ValidateError" title="Este campo es obligatorio"></span>',            
            cbxTipoMtto:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },             
            cbxTipoServicio:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxEntidad:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMunicipio:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtKmTotal: '<span class="ValidateError" title="Este campo es obligatorio y numerico"></span>',

    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });       
       
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });    
});
