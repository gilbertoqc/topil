$(document).ready(function(){ 
    // Inicializaci�n de los tabs
    $('#tabsForm').tabs({
        disabled: [1,2,3]
    });    
    
    // Barra de opciones flotante
    var barraOp = $('#dvBarraOpciones');
    var topBar = $(window).height() - (barraOp.height() + 7);
    var leftBar = (($(window).width() / 2) - (barraOp.width() / 2));
    barraOp.css({
        top: topBar,
        left: leftBar,
    });
    $(window).resize(function(){
        var topBar = $(this).height() - 60;
        var leftBar = (($(this).width() / 2) - (barraOp.width() / 2));
        barraOp.css({
            top: topBar,
            left: leftBar,
        });
    });
    
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1920:2002',
    });
    $('#txtFechaIng').datepicker({
        yearRange: '1950:',
    });
    
    // M�scara de los campos tel�fonos
    $('#txtTelFijo').mask('(999) 99-99999');
    $('#txtTelMovil').mask('(999) 99-99999');
    
        
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtRfc').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCuip').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCartilla').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtLicencia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtPasaporte').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
    $('#txtLugarNacimiento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCalle').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumExt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumInt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle1').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle2').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtColonia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCiudad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtInstitucion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCct').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCarrera').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEspecialidad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDocumento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtFolioDocto').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDocumento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    // Control especial del campo CURP.
    $('#txtCurp').blur(function(){
        $(this).val(xTrim( $(this).val().toUpperCase() ));
        if (msjErrorCURP != '') {
            shwError(msjErrorCURP);
        }
        else if ($(this).val() != '') {
            // Obtiene la fecha de nacimiento
            var fecha = $(this).val().substr(4, 6);
            $('#txtFechaNac').val(fecha.substr(4, 2) + '/' + fecha.substr(2, 2) + '/19' + fecha.substr(0, 2) );
            // Asigna el rfc autom�tico
            $("#txtRfc").val( $(this).val().substr(0, 10) );
            // Asigna el g�nero autom�tico
            var genero = $(this).val().substr(10, 1);  
            if (genero == "H")
                $("#rbnSexo1").attr('checked', 'checked');
            else if (genero == "M" )
                $("#rbnSexo2").attr('checked', 'checked');
        }            
    });
    
    // Control para la carga din�mica de municipios
    $('#cbxEntNacimiento').change(function(){        
        obtenerMunicipios('cbxMpioNacimiento', $(this).val());  
    });
    $('#cbxEntDomicilio').change(function(){
        obtenerMunicipios('cbxMpioDomicilio', $(this).val());  
    });
    $('#cbxRegion').change(function(){
        obtenerMunicipios('cbxMpioAdscripcion', $(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    jQuery.validator.addMethod("valCURP", function(value, element) {
        return validaCURP(value);
    });
    $('#frmRegistro').validate({
        rules:{
            // Datos Personales
            txtCurp:{
                required: true,
                minlength: 18,
                valCURP: true,    
            },
            txtNombre: 'required',            
            txtAPaterno: 'required',  
            txtAMaterno: 'required',
            rbnSexo: 'required',
            txtFechaNac: 'required',
            txtRfc: 'required',
            txtCuip:{
                minlength: 16,
            },
            txtFolioIfe: 'digits',
            cbxEdoCivil:{
                required: true,
                min: 1,
            },
            // Domicilio
            txtCalle: 'required',
            txtNumExt: 'required',
            txtColonia: 'required',
            txtCiudad: 'required',
            txtCodPostal:{ 
                digits: true,
                minlength: 5,
            },
            cbxEntDomicilio:{
                required: true,
                min: 1,
            },
            cbxMpioDomicilio:{
                required: true,
                min: 1,
            },
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: true,
                min: 1,
            },
            txtInstitucion: 'required',
            txtCedula:{
                digits: true,
                minlength: 7,
            },
            // Adscripci�n
    	},
    	messages:{
    	   txtCurp:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la CURP debe de ser de 18 caracteres"></span>',
                valCURP: '<span class="ValidateError" title="La CURP especificada no es v�lida"></span>',    
            },
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnSexo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaNac: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtRfc: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCuip:{ 
                minlength: '<span class="ValidateError" title="El tama�o de la CUIP debe de ser de al menos 16 caracteres"></span>',
            },
            txtFolioIfe: '<span class="ValidateError" title="Este campo s�lo acepta d�gitos"></span>',
            cbxEdoCivil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }, 
            // Domicilio
            txtCalle: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumExt: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtColonia: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCiudad: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCodPostal:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o del C�digo Postal debe de ser 5 digitos"></span>',
            },
            cbxEntDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },           
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtInstitucion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCedula:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la C�dula Profesional debe de ser 7 digitos"></span>',
            },
            // Adscripci�n
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
       
        
    // Acci�n del bot�n Siguiente
    $('#btnSiguiente').click(function(){        
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            // Se obtiene el total de tabs
            var totalTabs = $('#tabsForm >ul >li').size();            
            // Se obtien el id del tab actual activo
            var tabActual = $('#tabsForm').tabs('option', 'active'); 
            // Deshabilita todos los tabs
            $('#tabsForm').tabs('option', 'disabled', [0,1,2,3]);           
            // Se habilita y se muestra el siguiente tab
            $('#tabsForm').tabs('enable', (tabActual + 1));
            $('#tabsForm').tabs('option', 'active', (tabActual + 1));                        
            if (tabActual == (totalTabs - 2)) {            
                $('#btnSiguiente').css('display', 'none');
                $('#btnGuardar').css('display', 'inline-block');
            }            
            if (tabActual == 0)
                $('#btnAnterior').css('display', 'inline-block');            
        }
    });
    // Acci�n del bot�n Anterior
    $('#btnAnterior').click(function(){
        // Se obtien el id del tab actual activo
        var tabActual = $('#tabsForm').tabs('option', 'active');
        // Deshabilita todos los tabs
        $('#tabsForm').tabs('option', 'disabled', [0,1,2,3]);
        if (tabActual > 0){            
            $('#tabsForm').tabs('enable', (tabActual - 1));
            $('#tabsForm').tabs('option', 'active', (tabActual - 1));
        }
        if (tabActual == 3) {
            $('#btnGuardar').css('display', 'none');
            $('#btnSiguiente').css('display', 'inline-block');
        }
        else if (tabActual == 1) {
            $(this).css('display', 'none');
        }
    });
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });  
    
    /*-----------------------------------------------------------------//
    * Controla el scroll para seguir mostrando el header y el toolbar...
    -------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(window).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            $('#dvHeader').css('width', wWindow + 'px');
            toolbar.addClass('fixedToolBar');
            toolbar.css('width', (wWindow-13) + 'px');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');            
        }
    });
    $(window).on('resize', function(){
        var wWindow = $(this).width();
        $('#dvHeader').css('width', wWindow + 'px');
        $('#dvTool-Bar').css('width', (wWindow-13) + 'px');
    });  
});

function obtenerMunicipios(contenedor, id){
    var tp_filtro = ( contenedor != 'cbxMpioAdscripcion' ) ? 1 : 2;
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'id_region': id, 'filtro': tp_filtro},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}