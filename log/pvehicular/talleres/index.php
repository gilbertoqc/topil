<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
$objDatPer = new LogtblVehiculos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/talleres/index.js"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <!-- agregar un nuevo registro
                    <a href="index.php?m=<?php //echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('vehiculo_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo vehiculo...">
                        <img src="<?php //echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    -->
                    <!-- reportes -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Vehiculos" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">CLASIFICACION</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MARCA</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">TIPO</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">PLACAS</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">SERIE</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <!-- CONTENEDOR PARA LA LISTA DE MANTENIMIENTOS -------------------------------------------------------------------------- -->
    <div id="dvListaMtto" style="display: none;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td style="color: #153e7e; font-size: 14pt; font-weight: bold; padding: 7px 2px 7px 2px; text-align: center;">LISTA  DE MANTENIMIENTOS</td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr style="height: 27px;">
                    <th style="width: 10%;">&nbsp;</th>                    
                    <th style="font-size: 11pt; width: 30%;">FECHA INICIO</th>
                    <th style="font-size: 11pt; width: 40%;">FECHA FINALIZACI&Oacute;N</th>
                    <th style="width: 20%;">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div id="dvListMtto" class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 300px; overflow-y: hidden;">
        </div>
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('log/pvehicular/talleres/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlLista" value="<?php echo $objSys->encrypt('log/pvehicular/talleres/ajx_obt_lista_mtto.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>