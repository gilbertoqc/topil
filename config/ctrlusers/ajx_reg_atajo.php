<?php
/**
 * Complemento ajax para guardar el nuevo acceso a un módulo. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/xtblatajos.class.php';
    include $path . 'includes/class/xcatatajos.class.php';
    include $path . 'includes/class/xtblacceso.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objAtajos = new Xtblatajos();
    $objCatAtajos = new Xcatatajos();
    $objAcceso = new Xtblacceso();
    
    if( isset($_POST['oper']) && $_POST['oper'] == 'delete' ){
        $objAtajos->select($_SESSION["xIdUsuario"], $_POST['id_menu']);
        if( $objAtajos->delete() ){
            $oper = "Dlt";
            $id_reg = $_SESSION["xIdUsuario"] . '|' . $_POST['id_menu'];
            $objSys->registroLog($objUsr->idUsr, "xtblatajos", $id_reg, $oper);
            
            $ajx_datos['lista_atajos'] = $objCatAtajos->shwAtajos(0, "a.xstat = 1 AND a.id_menu NOT IN(Select id_menu From xtblatajos Where id_usuario = ".$_SESSION['xIdUsuario'].")");
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objAtajos->msjError;
        }

    } else {

        $objAtajos->id_usuario = $_SESSION["xIdUsuario"];
        $objAtajos->id_menu = $_POST["cbxAccesosDirectos"];
        $objAtajos->orden = $objAtajos->getMaxOrder() + 1;
                    
        $result = $objAtajos->insert();
        if ($result) {
            $oper = "Ins";
            $id_reg = $result;            
            $objSys->registroLog($objUsr->idUsr, "xtblatajos", $id_reg, $oper);

            // Se obtiene la lista de permisos especiales disponibles
            $ajx_datos['lista_atajos'] = $objCatAtajos->shwAtajos(0, "a.xstat = 1 AND a.id_menu NOT IN(Select id_menu From xtblatajos Where id_usuario = ".$_SESSION['xIdUsuario'].")");
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['html']  = '';
            $ajx_datos['error'] = $objAcceso->msjError;
        }

    }

    if( $ajx_datos['rslt'] ){
        // Se obtienen todos los accesos del usuario y se genera la lista
        $datos = $objAtajos->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "b.descripcion Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
        foreach( $datos As $reg => $dato ){
            $html .= '<tr id="dvGrid-Permisos-' . $dato["id_menu"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato['id_menu'] . '</td>';
                $html .= '<td style="text-align: left; width: 20%;">' . $dato["xcatatajos_descripcion"] . '</td>';
                $html .= '<td style="text-align: left; width: 30%;">' . $dato['xcatatajos_hint'] . '</td>';
                $html .= '<td style="text-align: left; width: 35%;">' . $objAcceso->getTreeMenu($dato["id_menu"]) . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_menu="' . $dato["id_menu"] . '" data-btn="delete" title="Eliminar acceso directo..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = $html;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>