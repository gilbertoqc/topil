$(document).ready(function(){ 
    
    setEditable(true);
    $('#btnModificar').click(function(evt){
        modificarUsuario('edit');
    });

    $('#btnAceptar').click(function(evt){
        modificarUsuario('save');
    });

    $('#btnCancelar').click(function(evt){
        modificarUsuario('cancell');
    });

    $('#btnEditPswd').click(function(evt){

    });

    $('#frmUsr').validate({
        rules:{
            txtNomUsr:{
                required: true,
            },
            txtNombre:{
                required: true,
            },
            txtFiltroIP:{
                required: true,
            },
        },
        messages:{    
            txtNomUsr:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNombre:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtFiltroIP:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    });

    // Convierte a mayúsculas el contenido de los textbox y textarea
    $('#txtNomUsr').blur(function(){ $(this).val(xTrim( $(this).val().toLowerCase() )) });
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtFiltroIP').blur(function(){ $(this).val(xTrim( $(this).val().toLowerCase() )) });

    $('#dvFormNewPswd').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 600,
        open: function(){
        },
        buttons: {
            'Guardar datos': function(){
                guardarPswd();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    $('#btnEditPswd').click(function(evt){
        $('#dvFormNewPswd').dialog('open');
        return false;
    });

    
    $('#tabs').tabs();
    /**---------------------------------------------------------------------------------------
     *-- Implementación del tab: Accesos
     *----------------------------------------------------------------------------------------*/
    // Aplicación de las reglas de validación de los campos del formulario
    $('#frmAcceso').validate({
        rules:{
            cbxMenu:{
                required: true,
                min: 1,
            },
            cbxPerfil:{
                required: true,
                min: 1,
            },
    	},
    	messages:{    
            cbxMenu:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxPerfil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormAcceso').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(1);
            getMenu();
        },
        buttons: {
            'Guardar datos': function(){
                guardarAcceso();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Controla el cambio de seleccion de perfil
    $('#cbxPerfil').live('change', function(){
        var permisos = $(this).find('option:selected').data('permisos');
        var html_permisos = permisos.replace(/\|/g, '<br />');
        $('#dvPermisosPerfil').html(html_permisos);
    });
    //-- Boton para agregar un nuevo acceso...
    $('#btnAddAcceso').click(function(){
        $('#dvFormAcceso').dialog('open');
        return false;
    });
    $('#dvGrid-Accesos a.lnkBtnOpcionGrid').live('click', function(){
        var this_obj = $(this);
        if( this_obj.data('btn') == 'delete' ){
            eliminarAcceso(this_obj.data('id_menu'));
        }
        return false;
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Accesos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Accesos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
    
    
    /*----------------------------------------------------------------------------------------
     *-- Implementación del tab: Permisos Especiales
     *----------------------------------------------------------------------------------------*/
    // Aplicación de las reglas de validación de los campos del formulario
    $('#frmPermiso').validate({
        rules:{
            cbxPermisos:{
                required: true,
                min: 1,
            },
    	},
    	messages:{    
            cbxPermisos:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormPermiso').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(2);
        },
        buttons: {
            'Guardar datos': function(){
                guardarPermiso();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Controla el cambio de seleccion de permisos
    $('#cbxPermisos').live('change', function(){
        var descrip = $(this).find('option:selected').data('descrip');
        $('#dvPermisoDetalle').html(descrip);
    });
    //-- Boton para agregar un nuevo permiso...
    $('#btnAddPermiso').click(function(){
        $('#dvFormPermiso').dialog('open');
        return false;
    });
    $('#dvGrid-Permisos a.lnkBtnOpcionGrid').live('click', function(){
        var this_obj = $(this);
        if( this_obj.data('btn') == 'delete' ){
            eliminarPermiso(this_obj.data('id_perm_usr'));
        }
        return false;
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Permisos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Permisos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
     
    /*----------------------------------------------------------------------------------------
     *-- Implementación del tab: Accesos Directos
     *----------------------------------------------------------------------------------------*/
    // Aplicación de las reglas de validación de los campos del formulario
    $('#frmAccesosDirectos').validate({
        rules:{
            cbxAccesosDirectos:{
                required: true,
                min: 1,
            },
        },
        messages:{    
            cbxAccesosDirectos:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        },
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormAccesosDirectos').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(3);
        },
        buttons: {
            'Guardar datos': function(){
                guardarAccesoDirecto();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Controla el cambio de seleccion de permisos
    $('#cbxAccesosDirectos').live('change', function(){
        var descrip = $(this).find('option:selected').data('descrip');
        $('#dvAtajoDetalle').html(descrip);
    });
    //-- Boton para agregar un nuevo acceso directo(atajo)...
    $('#btnAddAccesoDirecto').click(function(){
        $('#dvFormAccesosDirectos').dialog('open');
        return false;
    });
    $('#dvGrid-AccesosDirectos a.lnkBtnOpcionGrid').live('click', function(){
        var this_obj = $(this);
        if( this_obj.data('btn') == 'delete' ){
            eliminarAccesoDirecto(this_obj.data('id_menu'));
        }
        return false;
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
     
    /*----------------------------------------------------------------------------------------
     *-- Implementación del tab: Restricciones
     *----------------------------------------------------------------------------------------*/
    // Aplicación de las reglas de validación de los campos del formulario
    $('#frmRestricciones').validate({
        rules:{
            txtDescripcion:{
                required: true,
            },
            cbxTabla:{
                required: true,
            },
            cbxCampo:{
                required: true,
            },
            cbxOperador:{
                required: true,
            },
            txtValores:{
                required: true,
            }
        },
        messages:{
            txtDescripcion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTabla:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCampo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxOperador:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtValores:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormRestricciones').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(4);
        },
        buttons: {
            'Guardar datos': function(){
                guardarRestriccion();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Controla el cambio de seleccion de permisos
    $('#cbxTabla').live('change', function(){
        var tabla = $(this).find('option:selected').val();
        getCampos(tabla);
    });
    //-- Controla el cambio de seleccion de permisos
    $('#cbxCampo').live('change', function(){
        var tipo = $(this).find('option:selected').data('tipo');
        $('#hdnTipo').val(tipo);
    });
    //-- Boton para agregar un nuevo acceso directo(atajo)...
    $('#btnAddRestriccion').click(function(){
        $('#dvFormRestricciones').dialog('open');
        return false;
    });
    $('#dvGrid-Restricciones a.lnkBtnOpcionGrid').live('click', function(){
        var this_obj = $(this);
        if( this_obj.data('btn') == 'delete' ){
            eliminarRestriccion(this_obj.data('id_restriccion'));
        }
        return false;
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Restricciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Restricciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray'); 
    
    /*----------------------------------------------------------------------------------------
     *-- Implementación del tab: Logs
     *----------------------------------------------------------------------------------------*/
    //-- Ajusta el tamaño del contenedor del grid...
    //$('#dvGrid-Logs').css('height', (hWind - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Logs').xGrid({
        xColSort: 2,
        xTypeSort: 'Desc',
        xUrlData: xDcrypt($('#hdnUrlGetLogs').val()),
        xLoadDataStart: 1,
        xRowsDisplay: 35,
        xTypeDataAjax: 'json'
    });
});

function modificarUsuario(oper){
    if( oper == 'edit' ){
        setEditable(false);
    } else if( oper == 'save' ){
        saveDatUsr();
    } else if( oper == 'cancell' ){
        setEditable(true);

        $('#txtNomUsr').val( $('#txtNomUsr').data('value') );
        $('#txtNombre').val( $('#txtNombre').data('value') );
        $('#txtFiltroIP').val( $('#txtFiltroIP').data('value') );
        var id_perfil = $('#cbxPerfilGral').data('id');
        if( id_perfil == '' ) id_perfil = 0;
        $('#cbxPerfilGral').val(id_perfil);
    }
}

function saveDatUsr(){
    var valida = $('#frmUsr').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de aplicar estas modificaciones del usuario?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Aplicando los cambios, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlEditUsr').val()),
                        data: {
                            'oper': 'edit',
                            'nombre': $('#txtNombre').val(),
                            'nom_usr': $('#txtNomUsr').val(),
                            'filtro_ip': $('#txtFiltroIP').text(),
                            'id_perfil': $('#cbxPerfilGral').val(),
                        },
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                setEditable(true);

                                // Se actualizan los atributos data de los campos editables
                                $('#txtNomUsr').data('value', $('#txtNomUsr').val() );
                                $('#txtNombre').data('value', $('#txtNombre').val() );
                                $('#txtFiltroIP').data('value', $('#txtFiltroIP').text() );
                                $('#cbxPerfilGral').data('id', $('#cbxPerfilGral').val() );
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function setEditable(mode){
    if( typeof mode == 'undefined' )
        mode = true;

    $('#txtNomUsr').attr('readonly', mode);
    $('#txtNombre').attr('readonly', mode);
    $('#txtFiltroIP').attr('readonly', mode);
    $('#cbxPerfilGral option:not(:selected)').attr('disabled', mode);

    if( mode ){
        $('#cbxPerfilGral').addClass('readonly');

        $('#btnAceptar').css('display', 'none');
        $('#btnCancelar').css('display', 'none');
        $('#btnModificar').css('display', 'inline-block');
    } else {
        $('#cbxPerfilGral').removeClass('readonly');

        $('#btnAceptar').css('display', 'inline-block');
        $('#btnCancelar').css('display', 'inline-block');
        $('#btnModificar').css('display', 'none');
    }
}

function guardarPswd(){
    var pswd = $('#txtNewPswd').val();
    if( pswd.length >= 6 ){
        var frmPreg = $(getHTMLMensaje('¿Está seguro de modificar la contraseña del usuario?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Aplicando los cambios, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlEditUsr').val()),
                        data: {
                            'oper': 'pswd',
                            'pswd': pswd,
                        },
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                $('#dvFormNewPswd').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    } else {
        shwError('Por favor escriba una contraseña válida', 350);
    }
}

function guardarAcceso(){
    var valida = $('#frmAcceso').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de permitir el acceso seleccionado?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando el acceso, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveAcceso').val()),
                        data: $('#frmAcceso').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Accesos div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Accesos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Accesos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormAcceso').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function getMenu(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetMenu').val()),
        data: {},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            var html_menu = '<option value="0"></option>';
            var html_perfil = '<option value="0"></option>';
            if (xdata.rslt) {
                html_menu += xdata.menu;
                html_perfil += xdata.perfil;
                $('#cbxMenu').html(html_menu);
                $('#cbxPerfil').html(html_perfil);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function eliminarAcceso(id){
    var frmPreg = $(getHTMLMensaje('¿Está seguro de eliminar el acceso seleccionado?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 400,
        buttons:{
            'Aceptar': function(){
                // Se genera el diálogo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Eliminando el acceso, por favor espere...</div></div>');
                // Se procesan los datos vía ajax
                $.ajax({
                    url: xDcrypt($('#hdnUrlSaveAcceso').val()),
                    data: {
                        'oper': 'delete',
                        'id_menu': id,
                    },
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // Díalogo de espera                     
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        }); 
                    },
                    success: function (xdata) {
                        // Cierra el diálogo de espera
                        frmWait.dialog('close');
                        // Verifica el resultado del proceso
                        if (xdata.rslt) {
                            // Se actualiza el grid (lista) de referencia
                            $('#dvGrid-Accesos div.xGrid-dvBody').html(xdata.html);
                            //-- Se asigna el estilo intercalado de cada fila...
                            $('#dvGrid-Accesos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                            $('#dvGrid-Accesos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');                            
                        } else {
                            shwError('Error: ' + xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmWait.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}

function guardarPermiso(){
    var valida = $('#frmPermiso').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de asignar el permiso actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando el permiso especial, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSavePermiso').val()),
                        data: $('#frmPermiso').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Permisos div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Permisos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Permisos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se actualiza el combo con la lista de permisos especiales disponibles
                                $('#cbxPermisos').html('<option value="0"></option>').append(xdata.lista_permisos);
                                // Se cierra el formulario principal
                                $('#dvFormPermiso').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function eliminarPermiso(id){
    var frmPreg = $(getHTMLMensaje('¿Está seguro de eliminar el permiso seleccionado?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 400,
        buttons:{
            'Aceptar': function(){
                // Se genera el diálogo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Eliminando el permiso, por favor espere...</div></div>');
                // Se procesan los datos vía ajax
                $.ajax({
                    url: xDcrypt($('#hdnUrlSavePermiso').val()),
                    data: {
                        'oper': 'delete',
                        'id_perm_usr': id,
                    },
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // Díalogo de espera                     
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        }); 
                    },
                    success: function (xdata) {
                        // Cierra el diálogo de espera
                        frmWait.dialog('close');
                        // Verifica el resultado del proceso
                        if (xdata.rslt) {
                            // Se actualiza el grid (lista) de referencia
                            $('#dvGrid-Permisos div.xGrid-dvBody').html(xdata.html);
                            //-- Se asigna el estilo intercalado de cada fila...
                            $('#dvGrid-Permisos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                            $('#dvGrid-Permisos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');                            
                        } else {
                            shwError('Error: ' + xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmWait.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}

function guardarAccesoDirecto(){
    var valida = $('#frmPermiso').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de asignar el acceso directo actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando el acceso directo, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveAtajo').val()),
                        data: $('#frmAccesosDirectos').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-AccesosDirectos div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se actualiza el combo con la lista de accesos directos disponibles
                                $('#cbxAccesosDirectos').html('<option value="0"></option>').append(xdata.lista_atajos);
                                // Se cierra el formulario principal
                                $('#dvFormAccesosDirectos').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function eliminarAccesoDirecto(id){
    var frmPreg = $(getHTMLMensaje('¿Está seguro de eliminar el acceso directo seleccionado?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 400,
        buttons:{
            'Aceptar': function(){
                // Se genera el diálogo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Eliminando el acceso directo, por favor espere...</div></div>');
                // Se procesan los datos vía ajax
                $.ajax({
                    url: xDcrypt($('#hdnUrlSaveAtajo').val()),
                    data: {
                        'oper': 'delete',
                        'id_menu': id,
                    },
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // Díalogo de espera                     
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        }); 
                    },
                    success: function (xdata) {
                        // Cierra el diálogo de espera
                        frmWait.dialog('close');
                        // Verifica el resultado del proceso
                        if (xdata.rslt) {
                            // Se actualiza el grid (lista) de referencia
                            $('#dvGrid-AccesosDirectos div.xGrid-dvBody').html(xdata.html);
                            //-- Se asigna el estilo intercalado de cada fila...
                            $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                            $('#dvGrid-AccesosDirectos table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                            // Se actualiza el combo con la lista de accesos directos disponibles
                            $('#cbxAccesosDirectos').html('<option value="0"></option>').append(xdata.lista_atajos);
                        } else {
                            shwError('Error: ' + xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmWait.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}

function guardarRestriccion(){
    var valida = $('#frmRestricciones').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de asignar la restricción actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el diálogo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando la restricción, por favor espere...</div></div>');
                    // Se procesan los datos vía ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveRestriccion').val()),
                        data: $('#frmRestricciones').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // Díalogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el diálogo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Restricciones div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Restricciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Restricciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormRestricciones').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function getCampos(tabla){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetCampos').val()),
        data: {'tabla': tabla},
        type: 'post',
        dataType: 'json',
        async: true,
        beforeSend: function () {
            $('#cbxCampo').html('<option>Cargando...</option>');
        },
        success: function (xdata) {
            var html_campos = '<option value="0"></option>';
            if (xdata.rslt) {
                $('#cbxCampo').html(xdata.campos);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function eliminarRestriccion(id){
    var frmPreg = $(getHTMLMensaje('¿Está seguro de eliminar la restricción seleccionada?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 400,
        buttons:{
            'Aceptar': function(){
                // Se genera el diálogo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Eliminando la restricción, por favor espere...</div></div>');
                // Se procesan los datos vía ajax
                $.ajax({
                    url: xDcrypt($('#hdnUrlSaveRestriccion').val()),
                    data: {
                        'oper': 'delete',
                        'id_restriccion': id,
                    },
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // Díalogo de espera                     
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        }); 
                    },
                    success: function (xdata) {
                        // Cierra el diálogo de espera
                        frmWait.dialog('close');
                        // Verifica el resultado del proceso
                        if (xdata.rslt) {
                            // Se actualiza el grid (lista) de referencia
                            $('#dvGrid-Restricciones div.xGrid-dvBody').html(xdata.html);
                            //-- Se asigna el estilo intercalado de cada fila...
                            $('#dvGrid-Restricciones table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                            $('#dvGrid-Restricciones table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                        } else {
                            shwError('Error: ' + xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmWait.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}

function resetForm(form){
    if( form == 1 ){        
        $("#dvFormAcceso .validation span:not([class='pRequerido'])").hide();
        $("#dvFormAcceso .validation").removeClass("error");
        $("#dvFormAcceso .validation").removeClass("success");
    } else if( form == 2 ){
        $('#cbxPermisos').val(0);
                        
        $("#dvFormPermiso .validation span:not([class='pRequerido'])").hide();
        $("#dvFormPermiso .validation").removeClass("error");
        $("#dvFormPermiso .validation").removeClass("success");
    } else if( form == 3 ){
        $('#cbxAccesosDirectos').val(0);
        
        $("#dvFormAccesosDirectos .validation span:not([class='pRequerido'])").hide();
        $("#dvFormAccesosDirectos .validation").removeClass("error");
        $("#dvFormAccesosDirectos .validation").removeClass("success");
    } else if( form == 4 ){
        $('#txtDescripcion').val('');
        $('#cbxTabla').val(0);
        $('#cbxCampo').html('');
        $('#hdnTipo').val('');
        $('#cbxOperador').val(0);
        $('#txtValores').val('');
                
        $("#dvFormRestricciones .validation span:not([class='pRequerido'])").hide();
        $("#dvFormRestricciones .validation").removeClass("error");
        $("#dvFormRestricciones .validation").removeClass("success");
    }
}