<?php
/**
 * Complemento ajax para obtener el menú de arbol. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';    
    include $path . 'includes/class/xtblrestricciones.class.php';
    $objRestriccion = new Xtblrestricciones();
    
    if ($campos = $objRestriccion->getFields($_POST['tabla'])) {
        $ajx_datos['rslt']  = true;
        $html_campos = '';
        foreach ($campos as $campo) {
            $html_campos .= '<option value="' . $campo['campo'] . '" data-tipo="' . $campo['tipo'] . '">' . $campo['campo'] . '</option>';
        }
        $ajx_datos['campos'] = $html_campos;
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objRestriccion->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>