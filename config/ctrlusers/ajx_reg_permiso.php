<?php
/**
 * Complemento ajax para guardar el nuevo acceso a un módulo. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/xtblpermisos.class.php';
    include $path . 'includes/class/xcatpermisos.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objPermisos = new Xtblpermisos();
    $objCatPerm = new Xcatpermisos();
    
    if( isset($_POST['oper']) && $_POST['oper'] == 'delete' ){
        $objPermisos->select($_POST['id_perm_usr']);
        if( $objPermisos->delete() ){
            $oper = "Dlt";
            $id_reg = $_POST['id_perm_usr'];
            $objSys->registroLog($objUsr->idUsr, "xtblpermisos", $id_reg, $oper);
            
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objPermisos->msjError;
        }

    } else {

        $objPermisos->id_permiso = $_POST["cbxPermisos"];
        $objPermisos->id_usuario = $_SESSION["xIdUsuario"];
        $objPermisos->tipo = $_POST["rbTipoPerm"];
                    
        $result = $objPermisos->insert();
        if ($result) {
            $oper = "Ins";
            $id_reg = $result;            
            $objSys->registroLog($objUsr->idUsr, "xtblpermisos", $id_reg, $oper);

            // Se obtiene la lista de permisos especiales disponibles
            $ajx_datos['lista_permisos'] = $objCatPerm->shwPermisos(0, "a.tipo = 'Especial' AND a.id_permiso NOT IN(Select id_permiso From xtblpermisos Where id_usuario = ".$_SESSION['xIdUsuario'].")");
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['html']  = '';
            $ajx_datos['error'] = $objAcceso->msjError;
        }

    }

    if( $ajx_datos['rslt'] ){
        // Se obtienen todos los accesos del usuario y se genera la lista
        $datos = $objPermisos->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "b.permiso Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
        foreach( $datos As $reg => $dato ){
            $html .= '<tr id="dvGrid-Permisos-' . $dato["id_permiso_usr"] . '">';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato['id_permiso_usr'] . '</td>';
                $html .= '<td style="text-align: left; width: 25%;">' . $dato["xcatpermisos_permiso"] . '</td>';
                $html .= '<td style="text-align: left; width: 40%;">' . $dato['xcatpermisos_descripcion'] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato['tipo_perm'] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_perm_usr="' . $dato["id_permiso_usr"] . '" data-btn="delete" title="Eliminar permiso especial..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = $html;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>