<?php
/**
 * Complemento ajax para guardar el nuevo acceso a un módulo. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';    
    include $path . 'includes/class/xtblrestricciones.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objRestriccion = new Xtblrestricciones();
    
    if( isset($_POST['oper']) && $_POST['oper'] == 'delete' ){
        $objRestriccion->select($_POST['id_restriccion']);
        if( $objRestriccion->delete() ){
            $oper = "Dlt";
            $id_reg = $_POST['id_restriccion'];
            $objSys->registroLog($objUsr->idUsr, "xtblrestricciones", $id_reg, $oper);
            
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objRestriccion->msjError;
        }

    } else {

        $objRestriccion->id_restriccion = 0;
        $objRestriccion->descripcion = $_POST["txtDescripcion"];
        $objRestriccion->tabla = $_POST["cbxTabla"];
        $objRestriccion->campo = $_POST["cbxCampo"];
        $objRestriccion->tipo = $_POST["hdnTipo"];
        $objRestriccion->operador = $_POST["cbxOperador"];
        $objRestriccion->valor = $_POST["txtValores"];
        $objRestriccion->id_usuario = $_SESSION['xIdUsuario'];
                    
        $result = $objRestriccion->insert();
        if ($result) {
            $oper = "Ins";
            $id_reg = $result;            
            $objSys->registroLog($objUsr->idUsr, "xtblrestricciones", $id_reg, $oper);

            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['html']  = '';
            $ajx_datos['error'] = $objAcceso->msjError;
        }

    }

    if( $ajx_datos['rslt'] ){
        // Se obtienen todos los accesos del usuario y se genera la lista
        $datos = $objRestriccion->selectUser("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
        foreach( $datos As $reg => $dato ){
            $html .= '<tr id="dvGrid-Permisos-' . $dato["id_restriccion"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato['id_restriccion'] . '</td>';
                $html .= '<td style="text-align: left; width: 20%;">' . $dato["descripcion"] . '</td>';
                $html .= '<td style="text-align: left; width: 25%;">' . $dato['tabla'] . ' / ' . $dato['campo'] .'(' . $dato['tipo'] . ')' . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato['operador'] . '</td>';
                $html .= '<td style="text-align: left; width: 25%;">' . $dato['valor'] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_restriccion="' . $dato["id_restriccion"] . '" data-btn="delete" title="Eliminar la restricción..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = $html;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>