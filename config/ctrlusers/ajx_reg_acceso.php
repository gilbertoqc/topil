<?php
/**
 * Complemento ajax para guardar el nuevo acceso a un módulo. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/xtblacceso.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objAcceso = new Xtblacceso();    
    
    if( isset($_POST['oper']) && $_POST['oper'] == 'delete' ){
        $objAcceso->select($_SESSION['xIdUsuario'], $_POST['id_menu']);
        if( $objAcceso->delete() ){
            $oper = "Dlt";
            $id_reg = $_SESSION['xIdUsuario'] . '|' . $_POST['id_menu'];
            $objSys->registroLog($objUsr->idUsr, "xtblacceso", $id_reg, $oper);
            
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objAcceso->msjError;
        }

    } else {
        $objAcceso->id_usuario = $_SESSION["xIdUsuario"];
        $objAcceso->id_menu = $_POST["cbxMenu"];
        $objAcceso->id_perfil = $_POST["cbxPerfil"];
                
        $result = $objAcceso->insert();
        if ($result) {
            $oper = "Ins";
            $id_reg = $result;
            $objSys->registroLog($objUsr->idUsr, "xtblacceso", $id_reg, $oper);
            
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objAcceso->msjError;
        }

    }

    if( $ajx_datos['rslt'] ){
        // Se obtienen todos los accesos del usuario y se genera la lista
        $datos = $objAcceso->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "a.id_menu Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Accesos-tbBody">';
        foreach( $datos As $reg => $dato ){            
            $sty_color = ( $objAcceso->id_menu == $dato["id_menu"] ) ? 'color: #2b60de;' : '';            
            $html .= '<tr id="dvGrid-Accesos-' . $dato["id_menu"] . '">';
                $html .= '<td style="text-align: center; width: 10%; '. $sty_color .'">' . $dato['id_menu'] . '</td>';
                $html .= '<td style="text-align: left; width: 60%; '. $sty_color .'">' . $objAcceso->getTreeMenu($dato["id_menu"]) . '</td>';
                $html .= '<td style="text-align: center; width: 20%; '. $sty_color .'">' . $dato['xcatperfiles_perfil'] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_menu="' . $dato["id_menu"] . '" data-btn="delete" title="Eliminar acceso al módulo..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = $html;
    } else {
        $ajx_datos['html'] = '';
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>