<?php
/**
 * Complemento ajax para guardar el nuevo acceso a un módulo. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=utf-8');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/xtblusuarios.class.php';
    
    $objSys = new System();
    $objUsr = new Usuario();
    $objUsers = new Xtblusuarios();    
    
    $id_usr = isset($_POST['id']) ? $_POST['id'] : $_SESSION['xIdUsuario'];
    $objUsers->select($id_usr);

    if( $_POST['oper'] == 'edit' ){

        $objUsers->nombre = $_POST['nombre'];
        $objUsers->nom_usr = strtolower($_POST['nom_usr']);
        $objUsers->filtro_ip = strtolower($_POST['filtro_ip']);
        $objUsers->id_perfil = ($_POST['id_perfil'] > 0) ? $_POST['id_perfil'] : null;

        if( $objUsers->update() ){
            $objSys->registroLog($objUsr->idUsr, "xtblusuarios", $id_usr, 'Upd');
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objUsers->msjError;
        }

    } elseif( $_POST['oper'] == 'pswd' ){

        $objUsers->pswd = $_POST['pswd'];
        if( $objUsers->upd_pswd() ){
            $objSys->registroLog($objUsr->idUsr, "xtblusuarios", $id_usr, 'Upd');
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objUsers->msjError;
        }

    } elseif ( $_POST['oper'] == 'delete' ) {
        
        if( $objUsers->delete() ){
            $objSys->registroLog($objUsr->idUsr, "xtblusuarios", $id_usr, 'Dlt');
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objUsers->msjError;
        }

    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>