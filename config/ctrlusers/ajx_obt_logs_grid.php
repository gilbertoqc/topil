<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de parámetros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una búsqueda.
 * @param int colSort, contiene el índice de la columna por la cual se ordenarán los datos.
 * @param string typeSort, define el tipo de ordenación de los datos: ASC o DESC.
 * @param int rowStart, especifica el número de fila por la cual inicia la paginación del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrarán en cada página del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=utf-8');
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/xtblusuarios.class.php';
    $objSys = new System();
    $objUsers = new Xtblusuarios();

    //--------------------- Recepción de parámetros --------------------------//
    // Búsqueda...
    $sql_where = 'a.id_usuario = ?';
    $sql_values = array($_SESSION['xIdUsuario']);
    // Ordenación...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.id_log ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'a.fecha ' . $_GET['typeSort'] . ', a.hora ' . $_GET['typeSort'];
    } 
    // Paginación...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
    
    $datos = $objUsers->xtblLog()->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_log"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 8%;">' . $dato['id_log'] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato['fecha'] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["hora"] . '</td>';
                $html .= '<td style="text-align: center; width: 12%;">' . $dato['oper'] . '</td>';
                $html .= '<td style="text-align: left; width: 20%;">' . $dato['tabla'] . '</td>';
                $html .= '<td style="text-align: left; width: 18%;">' . $dato['id_reg'] . '</td>';
                $html .= '<td style="text-align: left; width: 12%;">' . $dato['ip'] . '</td>';
            $html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = $html;
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesión...";
}
?>