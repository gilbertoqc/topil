<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
$path = '';
include_once $path . 'includes/class/xtblusuarios.class.php';
$objUsers = new Xtblusuarios();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Configuración',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/datosrg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos    
    $objUsers->id_usuario = 0;
    $objUsers->nombre = strtoupper( $_POST['txtNombre'] );
    $objUsers->nom_usr = strtolower($_POST['txtNombreUsr']);
    $objUsers->pswd = $_POST['txtPswd'];
    $objUsers->id_perfil = ($_POST['cbxPerfil'] > 0) ? $_POST['cbxPerfil'] : null;
    $objUsers->filtro_ip = strtolower($_POST['txtFiltroIP']);
           
    //-------------------------------------------------------------------//            
    if ($id_nvo_usr = $objUsers->insert()) {
        $objSys->registroLog($objUsr->idUsr, 'xtblusuarios', $id_nvo_usr, "Ins");

        $_SESSION["xIdUsuario"] = $id_nvo_usr;
    } else {        
        $error = (!empty($objUsers->msjError)) ? $objUsers->msjError : 'Error al guardar los datos del usuario'; 
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("ctrl_user") : $objSys->encrypt("user_add"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>