<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/xtblusuarios.class.php';
include 'includes/class/xcatatajos.class.php';
include 'includes/class/xcatpermisos.class.php';
include 'includes/class/xtblrestricciones.class.php';

$objUsers = new Xtblusuarios();
$objCatAtajos = new Xcatatajos();
$objCatPerm = new Xcatpermisos();
$objRestriccion = new Xtblrestricciones();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Configuración',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                    '<script type="text/javascript" src="config/ctrlusers/_js/ctrluser.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <style type="text/css">
    .tdTituloTab{ color: #666362; font-style: italic; font-size: 10pt; font-weight: bold; padding-left: 5px; text-align: left; width: 70%; }
    #dvAction-Btns{ position: relative; display: inline-table; margin-right: 30px; }
    .dvDetallesItem{ background-color: #fafafa; border-top: 1px inset white; border-left: 1px inset white; border-right: 1px inset gray; border-bottom: 1px inset gray; color: gray; display: block; font-size: 9pt;  min-height: 50px; min-width: 100px; padding: 4px 6px; }
    </style>

    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php                    
                    $urlBack = 'index.php?m=' . $_SESSION['xIdMenu'];
                    ?>
                    <!-- Botones de opción... -->
                    <div id="dvAction-Btns">
                        <a href="#" id="btnModificar" class="Tool-Bar-Btn gradient" style="" title="Modificar los datos generales del usuario...">
                            <img src="includes/css/imgs/icons/edit24.png" alt="" style="border: none;" /><br />Modificar
                        </a>
                        <a href="#" id="btnAceptar" class="Tool-Bar-Btn gradient" style="display: none;" title="Aplicar modificaciones...">
                            <img src="includes/css/imgs/icons/ok24.png" alt="" style="border: none;" /><br />Aceptar
                        </a>
                        <a href="#" id="btnCancelar" class="Tool-Bar-Btn gradient" style="display: none;" title="Cancelar modificaciones...">
                            <img src="includes/css/imgs/icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                        </a>
                    </div>
                    <a href="<?php echo $urlBack?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="" title="Regresar...">
                        <img src="includes/css/imgs/icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION["xIdUsuario"] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION["xIdUsuario"];
    $objUsers->select($_SESSION["xIdUsuario"]);    
    $foto = ( !empty($objUsers->foto) ) ? 'adm/expediente/fotos/' . $objUsers->foto : '';
    if( empty($foto) )
        $foto = 'adm/expediente/sin_foto_h.jpg';
    ?>    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 1000px;">
        <span class="dvForm-Data-pTitle"><img src="includes/css/imgs/icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Perfil de usuario</span>
        
        <fieldset class="fsetForm-Data" style="width: 900px;">
            <legend>Datos Generales</legend>   
            <form id="frmUsr" method="post" action="#" enctype="multipart/form-data">
                <table class="tbForm-Data" style="table-layout: fixed;" border="0">
                    <tr>
                        <td rowspan="12" style="text-align: center; width: 290px; vertical-align: top;">
                            <div class="dvFoto-Normal" style="background-image: url(<?php echo $foto;?>);vertical-align: bottom;"></div>
                        </td>
                    </tr>
                    <tr>                        
                        <td style="width: 165px;"><label for="txtIDUsuario">ID de Usuario:</label></td>
                        <td class="control-group" style="width: 480px;">
                            <input type="text" name="txtIDUsuario" id="txtIDUsuario" value="<?php echo $objUsers->id_usuario;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>                        
                        <td style="width: 145px;"><label for="txtNomUsr">Usuario:</label></td>
                        <td class="validation" style="width: 450px;">
                            <input type="text" name="txtNomUsr" id="txtNomUsr" value="<?php echo $objUsers->nom_usr;?>" data-value="<?php echo $objUsers->nom_usr;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNombre">Nombre Completo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $objUsers->nombre;?>" data-value="<?php echo $objUsers->nombre;?>" title="..." readonly="true" style="width: 350px;" />
                        </td>
                    </tr>                    
                    <tr>
                        <td><label for="txtFiltroIP">Filtro por IP:</label></td>
                        <td class="control-group">
                            <textarea name="txtFiltroIP" id="txtFiltroIP" data-value="<?php echo $objUsers->filtro_ip;?>" title="..." readonly="true" style="heigth: 60px; width: 180px; max-width: 250px; max-height: 80px;" ><?php echo $objUsers->filtro_ip;?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxPerfilGral">Perfil General:</label></td>
                        <td class="validation">
                            <select name="cbxPerfilGral" id="cbxPerfilGral" data-id="<?php echo $objUsers->id_perfil; ?>">
                                <option value="0">- Perfiles -</option>
                                <?php
                                echo $objUsers->Xcatperfiles->shwPerfiles($objUsers->id_perfil, "a.tipo = 'General'");
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPswd">Contraseña:</label></td>
                        <td class="control-group" style="width: 450px;">
                            <input type="text" name="txtPswd" id="txtPswd" value="***********" title="..." readonly="true" style="width: 200px;" />
                            <a href="#" id="btnEditPswd" class="lnkBtnOpcionGrid" style="min-height: 18px; min-width: 18px;">
                                <img src="<?php echo PATH_IMAGES;?>icons/edit16.png" alt="edit_pswd" title="Modificar contraseña" />
                            </a>
                            <div id="dvFormNewPswd" title="SISP :: Modificar contraseña">                                
                                <table class="tbForm-Data">
                                    <tr>
                                        <td><label for="txtNewPswd">Nueva Contraseña:</label></td>
                                        <td class="validation">
                                            <input type="text" name="txtNewPswd" id="txtNewPswd" value="" data-value="" title="..." style="width: 250px;" />
                                            <span class="pRequerido">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaExpira">Expiración contraseña:</label></td>
                        <td class="control-group">
                            <input type="text" name="txtFechaExpira" id="txtFtxtFechaExpiraechaEdit" value="<?php echo $objUsers->fecha_expira;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtStatus">Status:</label></td>
                        <td class="control-group">
                            <?php
                            $status = ($objUsers->stat == 1) ? 'Activo' : 'Inactivo';
                            ?>
                            <input type="text" name="txtStatus" id="txtStatus" value="<?php echo $status;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaReg">Fecha de registro:</label></td>
                        <td class="control-group">                                        
                            <input type="text" name="txtFechaReg" id="txtFechaReg" value="<?php echo $objUsers->fecha_reg;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>   
                    <tr>
                        <td><label for="txtFechaEdit">Última modificación:</label></td>
                        <td class="control-group">
                            <input type="text" name="txtFechaEdit" id="txtFechaEdit" value="<?php echo $objUsers->fecha_edit;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtUltAcceso">Último acceso:</label></td>
                        <td class="control-group">
                            <input type="text" name="txtUltAcceso" id="txtUltAcceso" value="<?php echo $objUsers->ultimo_acceso;?>" title="..." readonly="true" style="width: 200px;" />
                        </td>
                    </tr>                    
                </table>
            </form>
            <input type="hidden" id="hdnUrlEditUsr" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_upd_usr.php');?>" />
        </fieldset>
                    
        <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
            <ul>
                <li><a href="#tabs-1" style="font-size: 11pt; min-width: 150px; text-align: center;">Módulos</a></li>
                <li><a href="#tabs-2" style="font-size: 11pt; min-width: 150px; text-align: center;">Permisos</a></li>                
                <li><a href="#tabs-3" style="font-size: 11pt; min-width: 150px; text-align: center;">Accesos directos</a></li>
                <li><a href="#tabs-4" style="font-size: 11pt; min-width: 150px; text-align: center;">Restricciones</a></li>
                <li><a href="#tabs-5" style="font-size: 11pt; min-width: 150px; text-align: center;">Logs</a></li>
            </ul>         
            <!-- Accesos a Menus -->
            <div id="tabs-1">    
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Módulos disponibles para el usuario</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddAcceso" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Asignar módulo...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>                 
                <div id="dvGrid-Accesos" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th class="xGrid-tbCols-ColSortable" style="width: 10%;">ID</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 60%;">MÓDULO</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">PERFIL</th>
                                <th style="width: 10%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objUsers->xtblAcceso()->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "a.id_menu Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Accesos-tbBody" style="">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Accesos-' . $dato["id_menu"] . '">';
                                $html .= '<td style="text-align: center; width: 10%;">' . $dato['id_menu'] . '</td>';
                                $html .= '<td style="text-align: left; width: 60%;">' .$objUsers->xtblAcceso()->getTreeMenu($dato["id_menu"]) . '</td>';
                                $html .= '<td style="text-align: center; width: 20%;">' . $dato['xcatperfiles_perfil'] . '</td>';
                                $html .= '<td style="text-align: center; width: 10%;">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_menu="' . $dato["id_menu"] . '" data-btn="delete" title="Eliminar acceso al módulo..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }
                    ?>    
                    </div>
                </div>
                
                <div id="dvFormAcceso" title="SISP :: Nuevo Acceso a módulos">
                    <form id="frmAcceso" method="post" action="#" enctype="multipart/form-data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="cbxMenu">Menú de módulos:</label></td>
                                <td class="validation">
                                    <select name="cbxMenu" id="cbxMenu" style="max-width: 450px;">
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxPerfil">Perfil:</label></td>
                                <td class="validation">
                                    <select name="cbxPerfil" id="cbxPerfil" style="max-width: 450px;">
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-style: italic; color: gray;">Permisos:</td>
                                <td>
                                    <div id="dvPermisosPerfil" class="dvDetallesItem"></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveAcceso" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_reg_acceso.php');?>" />
                <input type="hidden" id="hdnUrlGetMenu" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_obt_menu.php');?>" />
            </div>
            
            <!-- Permisos -->
            <div id="tabs-2">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Permisos Especiales del Usuario</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddPermiso" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Asignar un permiso especial...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Permisos" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th class="xGrid-tbCols-ColSortable" style="width: 10%;">ID</th> 
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">PERMISO</th> 
                                <th class="xGrid-tbCols-ColSortable" style="width: 40%;">DESCRIPCIÓN</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">TIPO</th>
                                <th style="width: 10%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objUsers->xtblPermisos()->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "b.permiso Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Permisos-' . $dato["id_permiso_usr"] . '">';
                                $html .= '<td style="text-align: center; width: 10%;">' . $dato['id_permiso_usr'] . '</td>';
                                $html .= '<td style="text-align: left; width: 25%;">' . $dato["xcatpermisos_permiso"] . '</td>';
                                $html .= '<td style="text-align: left; width: 40%;">' . $dato['xcatpermisos_descripcion'] . '</td>';
                                $html .= '<td style="text-align: center; width: 15%;">' . $dato['tipo_perm'] . '</td>';
                                $html .= '<td style="text-align: center; width: 10%;">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_perm_usr="' . $dato["id_permiso_usr"] . '" data-btn="delete" title="Eliminar permiso especial..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }
                    ?>                         
                    </div>
                </div>
                
                <div id="dvFormPermiso" title="SISP :: Nuevo Permiso Especial">
                    <form id="frmPermiso" method="post" action="#" enctype="multipart/form-data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="cbxPermisos">Permiso Especial:</label></td>
                                <td class="validation">
                                    <select name="cbxPermisos" id="cbxPermisos" style="max-width: 300px;"> 
                                        <option value="0"></option>
                                        <?php
                                        echo $objCatPerm->shwPermisos(0, "a.tipo = 'Especial' AND a.id_permiso NOT IN(Select id_permiso From xtblpermisos Where id_usuario = ".$_SESSION['xIdUsuario'].")");
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-style: italic; color: gray;">Detalles:</td>
                                <td>
                                    <div id="dvPermisoDetalle" class="dvDetallesItem" style="min-height: 17px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="">Tipo:</label></td>
                                <td class="validation">
                                    <label class="label-Radio"><input type="radio" name="rbTipoPerm" value="1" checked="true"> Permitido</label>
                                    <label class="label-Radio" style="margin-left: 15px;"><input type="radio" name="rbTipoPerm" value="0"> Restringido</label>
                                </td>
                            </tr>
                        </table>                    
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSavePermiso" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_reg_permiso.php');?>" />
            </div>
            
            <!-- Accesos Directos -->
            <div id="tabs-3">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Accesos Directos del Usuario</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddAccesoDirecto" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar un acceso directo para el usuario...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-AccesosDirectos" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">ID</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">ACCESO</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 30%;">DESCRIPCIÓN</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 35%;">RUTA</th>
                                <th style="width: 10%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objUsers->xtblAtajos()->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "b.descripcion Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Permisos-' . $dato["id_menu"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . $dato['id_menu'] . '</td>';
                                $html .= '<td style="text-align: left; width: 20%;">' . $dato["xcatatajos_descripcion"] . '</td>';
                                $html .= '<td style="text-align: left; width: 30%;">' . $dato['xcatatajos_hint'] . '</td>';
                                $html .= '<td style="text-align: left; width: 35%;">' . $objUsers->xtblAcceso()->getTreeMenu($dato["id_menu"]) . '</td>';
                                $html .= '<td style="text-align: center; width: 10%;">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_menu="' . $dato["id_menu"] . '" data-btn="delete" title="Eliminar acceso directo..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }
                    ?>  
                    </div>
                </div>
                <div id="dvFormAccesosDirectos" title="SISP :: Nuevo Acceso Directo">
                    <form id="frmAccesosDirectos" method="post" action="#" enctype="multipart/form-data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="cbxAccesosDirectos">Acceso Directo:</label></td>
                                <td class="validation">
                                    <select name="cbxAccesosDirectos" id="cbxAccesosDirectos" style="max-width: 300px;"> 
                                        <option value="0"></option>                               
                                        <?php
                                        echo $objCatAtajos->shwAtajos(0, "a.xstat = 1 AND a.id_menu NOT IN(Select id_menu From xtblatajos Where id_usuario = ".$_SESSION['xIdUsuario'].")");
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-style: italic; color: gray;">Descripción:</td>
                                <td>
                                    <div id="dvAtajoDetalle" class="dvDetallesItem" style="min-height: 17px;"></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveAtajo" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_reg_atajo.php');?>" />
            </div>

            <!-- Restricciones -->
            <div id="tabs-4">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Restricciones a Nivel de Datos</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddRestriccion" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar una nueva restricción...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Restricciones" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">DESCRIPCIÓN</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">TABLA/CAMPO</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 10%;">OPERADOR</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">VALORE(S)</th>
                                <th style="width: 10%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    // Se obtienen todos los accesos del usuario y se genera la lista
                    $datos = $objRestriccion->selectUser("a.id_usuario=" . $_SESSION['xIdUsuario']);
                    $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
                    foreach( $datos As $reg => $dato ){
                        $html .= '<tr id="dvGrid-Permisos-' . $dato["id_restriccion"] . '">';
                            $html .= '<td style="text-align: center; width: 5%;">' . $dato['id_restriccion'] . '</td>';
                            $html .= '<td style="text-align: left; width: 20%;">' . $dato["descripcion"] . '</td>';
                            $html .= '<td style="text-align: left; width: 25%;">' . $dato['tabla'] . ' / ' . $dato['campo'] .'(' . $dato['tipo'] . ')' . '</td>';
                            $html .= '<td style="text-align: center; width: 10%;">' . $dato['operador'] . '</td>';
                            $html .= '<td style="text-align: left; width: 25%;">' . $dato['valor'] . '</td>';
                            $html .= '<td style="text-align: center; width: 10%;">';
                            $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id_restriccion="' . $dato["id_restriccion"] . '" data-btn="delete" title="Eliminar la restricción..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="eliminar" /></a>';
                            $html .= '</td>';
                        $html .= '</tr>';
                    }
                    $html .= '</table>';
                    echo $html;
                    ?>
                    </div>
                </div>
                <div id="dvFormRestricciones" title="SISP :: Nueva Restricción">
                    <form id="frmRestricciones" method="post" action="#" enctype="multipart/form-data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="txtDescripcion">Descripción:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtDescripcion" id="txtDescripcion" value="" style="width: 350px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxTabla">Tabla:</label></td>
                                <td class="validation">
                                    <select name="cbxTabla" id="cbxTabla" style="max-width: 300px;"> 
                                        <option value="0"></option>                               
                                        <?php
                                        $tables = $objRestriccion->getTables();
                                        foreach ($tables as $table) {
                                            echo '<option value="' . $table . '">' . $table . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxCampo">Campo:</label></td>
                                <td class="validation">
                                    <select name="cbxCampo" id="cbxCampo" style="max-width: 300px;"> 
                                        <option value="0"></option>
                                    </select>
                                    <span class="pRequerido">*</span>
                                    <input type="hidden" name="hdnTipo" id="hdnTipo" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxOperador">Operador:</label></td>
                                <td class="validation">
                                    <select name="cbxOperador" id="cbxOperador" style="max-width: 300px;"> 
                                        <option value="0"></option>
                                        <?php
                                        $operators = $objRestriccion->getOperators();
                                        foreach ($operators as $operator) {
                                            echo '<option value="' . $operator . '">' . $operator . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtValores">Valor(es):</label></td>
                                <td class="validation">
                                    <input type="text" name="txtValores" id="txtValores" value="" style="width: 350px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveRestriccion" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_reg_restriccion.php');?>" />
                <input type="hidden" id="hdnUrlGetCampos" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_obt_campos.php');?>" />
            </div>
            
            <!-- Logs -->
            <div id="tabs-5">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Registro de Actividades del Usuario</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                &nbsp;
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Logs" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: 948px;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 8%;">ID</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">FECHA</th>
                                <th class="xGrid-tbCols" style="width: 10%;">HORA</th>
                                <th class="xGrid-tbCols" style="width: 12%;">OPERACIÓN</th>
                                <th class="xGrid-tbCols" style="width: 20%;">TABLA</th>
                                <th class="xGrid-tbCols" style="width: 18%;">REGISTRO</th>
                                <th class="xGrid-tbCols" style="width: 12%;">IP</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody">
                    <?php
                    // $datos = $objUsers->xtblLog()->selectAll("a.id_usuario='" . $_SESSION['xIdUsuario'] . "'", "a.fecha Desc, a.hora Desc");
                    // if( count($datos) > 0 ){
                    //     $html = '<table class="xGrid-tbBody" id="dvGrid-Permisos-tbBody" style="">';
                    //     foreach( $datos As $reg => $dato ){
                    //         $html .= '<tr id="dvGrid-Permisos-' . $dato["id_log"] . '">';
                    //             $html .= '<td style="text-align: center; width: 8%;">' . $dato['id_log'] . '</td>';
                    //             $html .= '<td style="text-align: center; width: 15%;">' . $dato['fecha'] . '</td>';
                    //             $html .= '<td style="text-align: center; width: 15%;">' . $dato["hora"] . '</td>';
                    //             $html .= '<td style="text-align: center; width: 15%;">' . $dato['oper'] . '</td>';
                    //             $html .= '<td style="text-align: left; width: 20%;">' . $dato['tabla'] . '</td>';
                    //             $html .= '<td style="text-align: left; width: 15%;">' . $dato['id_reg'] . '</td>';
                    //             $html .= '<td style="text-align: left; width: 12%;">' . $dato['ip'] . '</td>';
                    //         $html .= '</tr>';
                    //     }
                    //     $html .= '</table>';
                    //     echo $html;
                    // }
                    ?> 
                    </div>
                    <input type="hidden" id="hdnUrlGetLogs" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_obt_logs_grid.php');?>" />
                </div>
                <br />
            </div>
        </div>
    </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>