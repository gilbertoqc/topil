<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_cursos.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objCurso = new AdmtblCursos();    
    
    $idCurso = $_POST["hdnIdCurso"];
    $objCurso->id_curso = $idCurso;
    $objCurso->curp = $_SESSION["xCurp"];
    $objCurso->id_tipo_curso = $_POST["cbxTipoCurso"];
    $objCurso->curso = utf8_decode($_POST["txtNombreCurso"]);
    $objCurso->institucion = utf8_decode($_POST["txtInstitucion"]);
    $objCurso->eficiencia_term = $_POST["rbnEfiTerm"];
    $objCurso->fecha_ini = $objSys->convertirFecha($_POST["txtFechaIniCurso"], "yyyy-mm-dd");
    $objCurso->fecha_fin = $objSys->convertirFecha($_POST["txtFechaFinCurso"], "yyyy-mm-dd");
    $objCurso->duracion = $_POST["txtDuracionCurso"];
    $objCurso->observaciones = utf8_decode($_POST["txtObservaCurso"]);
            
    $result = ($_POST["hdnTypeOperCurso"] == 1) ? $objCurso->insert() : $objCurso->update();
    if ($result) {
        $ajx_datos['rslt']  = true;
        if( $_POST["hdnTypeOperCurso"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;    
        } else {
            $oper = "Edt";
            $id_reg = $idCurso;
        }        
        $objSys->registroLog($objUsr->idUsr, "admtbl_cursos", $id_reg, $oper);
        // Se obtienen todos los cursos de la persona y se genera la lista
        $datos = $objCurso->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_ini Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Cursos-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idCurso == $dato["id_curso"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_curso"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Cursos-' . $dato["id_curso"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                $html .= '<td style="text-align: left; width: 25%; ' . $sty_color . '">' . $dato["curso"] . '</td>';
                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $dato["tipo_curso"] . '</td>';
                if( $dato["eficiencia_term"] == 1 )
                    $ef_term = "COMPLETADO";                            
                else if( $dato["eficiencia_term"] == 2 )
                    $ef_term = "CURSANDO";
                else if( $dato["eficiencia_term"] == 3 )
                    $ef_term = "INCOMPLETO";
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $ef_term . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_ini"])) . '</td>';            
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_fin"])) . '</td>';
                $html .= '<td style="text-align: center; width: 8%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_curso"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objCurso->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>