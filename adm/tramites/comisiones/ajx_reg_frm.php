<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_cambios_temporales.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objCambiosTemp  = new AdmtblCambiosTemporales();
    
    //Recibe la variables por POST 
    //Convierte a codificacion iso-8859
    //asigna el nombre de variable a la variable recibida
    foreach($_POST as $nombre_campo => $valor){ 
        $asignacion = "\$" . $nombre_campo . "='" . utf8_decode($valor) . "';"; 
        eval($asignacion); 
    }
    
    $curp = $objSys->decrypt( $_POST["curp"] ); 
    
    // Datos de los Cambios
    $objCambiosTemp->curp = $curp;
    $objCambiosTemp->idcambio_temp = $_POST["id_tramite"];
    $objCambiosTemp->fecha_inicio = ($_POST["txtFechaInicio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaInicio"], 'yyyy-mm-dd');
    $objCambiosTemp->fecha_fin = ($_POST["txtFechaFin"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaFin"], 'yyyy-mm-dd');
    $objCambiosTemp->id_area = $_POST["cbxArea"];
    $objCambiosTemp->id_municipio = $_POST["cbxMpioAdscripcion"];
    $objCambiosTemp->id_tipo_cambio = $_POST["cbxTipoCamb"];
    $objCambiosTemp->id_ubicacion = $_POST["cbxUbicacion"];
    // Datos del Oficio
    $objCambiosTemp->no_oficio = $txtNoOficio;
    $objCambiosTemp->fecha_oficio = ($_POST["txtFechaOficio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaOficio"], 'yyyy-mm-dd');
    $objCambiosTemp->no_soporte =$txtNoSoporte;
    $objCambiosTemp->fecha_soporte = ($_POST["txtFechaSoporte"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaSoporte"], 'yyyy-mm-dd');
    $objCambiosTemp->firmante_soporte = $txtFirmante;
    $objCambiosTemp->cargo_firmante = $txtCargoFirmante;
    
   
    $result = ($_POST["dtTypeOper"] == 1) ? $objCambiosTemp->update() : $objCambiosTemp->insert();
    $oper = $_POST["dtTypeOper"] == '1' ? 'Edt' : 'Ins';
    
   if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_cambios_temporales', $curp, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
       // $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objCambiosTemp->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}

?>