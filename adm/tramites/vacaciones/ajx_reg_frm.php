<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_vacaciones.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objVacaciones  = new AdmtblVacaciones();
    
    //Recibe la variables por POST 
    //Convierte a codificacion iso-8859
    //asigna el nombre de variable a la variable recibida
    foreach($_POST as $nombre_campo => $valor){ 
        $asignacion = "\$" . $nombre_campo . "='" . utf8_decode($valor) . "';"; 
        eval($asignacion); 
    }
    
    
    $curp = $objSys->decrypt( $_POST["curp"] ); 
    
    // Datos de los Cambios
    $objVacaciones->curp = $curp;
    $objVacaciones->folio = $txtNoFolio;
    $objVacaciones->anio = $_POST["cbxAnioPer"];
    $objVacaciones->fecha_ini = ($_POST["txtFechaInicio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaInicio"], 'yyyy-mm-dd');
    $objVacaciones->fecha_fin = ($_POST["txtFechaFin"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaFin"], 'yyyy-mm-dd');
    $objVacaciones->periodo = $_POST["rbnPeriodo"];
    $objVacaciones->num_dias = $_POST["txtDias"];
    $objVacaciones->observaciones = $txtObservaciones;
    
    // Datos del Oficio
    $objVacaciones->no_oficio = $txtNoOficio;
    $objVacaciones->fecha_oficio = ($_POST["txtFechaOficio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaOficio"], 'yyyy-mm-dd');
    $objVacaciones->no_soporte =$txtNoSoporte;
    $objVacaciones->fecha_soporte = ($_POST["txtFechaSoporte"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaSoporte"], 'yyyy-mm-dd');
    $objVacaciones->firmante_soporte = $txtFirmante;
    $objVacaciones->cargo_firmante = $txtCargoFirmante;
    
   
    $result = ($_POST["dtTypeOper"] == 1) ? $objVacaciones->update() : $objVacaciones->insert();
    $oper = $_POST["dtTypeOper"] == '1' ? 'Edt' : 'Ins';
    
   if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_vacaciones', $curp, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
       // $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objVacaciones->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}

?>