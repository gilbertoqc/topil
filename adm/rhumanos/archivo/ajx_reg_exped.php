<?php
/**
 * Complemento ajax para guardar el n�mero de expediente. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_archivo.class.php';
       
    $objSys = new System();
    $objUsr = new Usuario();    
    $objArchivo = new AdmtblArchivo();
    
    $objArchivo->curp = $_SESSION["xCurp"];
    $objArchivo->expediente = $_POST["exped"];
    $objArchivo->status = 1;
       
    if ($objArchivo->ctrl_archivo()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_archivo', $_SESSION["xCurp"], "Edt");
        $ajx_datos['rslt']  = true;
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objArchivo->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>