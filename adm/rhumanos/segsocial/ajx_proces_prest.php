<?php
/**
 * Complemento ajax para procesar los datos de una prestación. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_prestaciones.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objPrest = new AdmtblPrestaciones();    
    
    $idPrestPer = $_POST["hdnIdPrestPer"];
    $objPrest->id_prestacion_personal = $idPrestPer;
    $objPrest->curp = $_SESSION["xCurp"];
    $objPrest->id_prestacion = ( $_POST["hdnTypeOperPrest"] == 1 ) ? $_POST["cbxPrestaciones"] : $_POST["hdnIdPrestacion"];
    $objPrest->id_status_prest = $_POST["cbxStatusPrest"];
    $objPrest->fecha_alta = (!empty($_POST["txtFechaAlta"])) ? $objSys->convertirFecha($_POST["txtFechaAlta"], "yyyy-mm-dd") : null;
    $objPrest->observaciones = utf8_decode($_POST["txtObservaPrest"]);
    
    if( $_POST["hdnTypeOperPrest"] == 1 ){
        $result = $objPrest->insert();
        $oper = "Ins";
        $id_reg = $result;
    } else if( $_POST["hdnTypeOperPrest"] == 2 ){
        $result = $objPrest->update();
        $oper = "Edt";
        $id_reg = $idPrestPer;
    } else if( $_POST["hdnTypeOperPrest"] == 3 ){
        $result = $objPrest->delete();
        $oper = "Dlt";
        $id_reg = $idPrestPer;
    }
       
    if ($result) {
        $ajx_datos['rslt']  = true;
                
        $objSys->registroLog($objUsr->idUsr, "admtbl_prestaciones", $id_reg, $oper);
        
        // Se obtienen todas las prestaciones de la persona y se genera la lista
        $datos = $objPrest->selectAll("a.curp='" . $_SESSION['xCurp'] . "'");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Prest-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idPrestPer == $dato["id_prestacion_personal"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_prestacion_personal"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Prest-' . $dato["id_prestacion_personal"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                $html .= '<td style="text-align: left; width: 40%; ' . $sty_color . '">' . $dato["prestacion"] . '</td>';
                $fecha_alta = (!empty($dato["fecha_alta"])) ? date("d/m/Y", strtotime($dato["fecha_alta"])) : '';
                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $fecha_alta . '</td>';            
                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . $dato["status_prest"] . '</td>';
                $html .= '<td style="text-align: center; width: 18%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_prestacion_personal"] . '" title="Moficar esta prestación..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objPrest->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>