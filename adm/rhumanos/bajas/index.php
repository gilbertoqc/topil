<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_bajas.class.php';
$objBajas = new AdmtblBajas();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/bajas/_js/index.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    unset($_SESSION["xIdBaja"]);
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <a href="#" id="xRegistrar" class="Tool-Bar-Btn" style="width: 100px;" title="Registrar un nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/personal_dlt24.png" alt="" style="border: none;" /><br />Nueva baja
                    </a>
                    <a href="#" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Bajas" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; min-width: 1200px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 6%;" class="xGrid-tbCols-ColSortable">FOLIO</th>
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 14%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 14%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">NO. OFICIO</th>
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MOTIVO BAJA</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">FECHA BAJA</th>
                    <th style="width: 12%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <div id="dvForm-Baja" title="">
        <form id="frmBaja" method="post" action="#" enctype="multipart/form-data">       
            <fieldset class="fsetForm-Data">
                <legend>Datos del empleado</legend>
                <table class="tbForm-Data">  
                    <tr>
                        <td><label for="txtCurp">C.U.R.P.:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCurp" id="txtCurp" value="" readonly="true" title="" style="width: 195px;" />
                            <span class="pRequerido">*</span>
                            <a href="#" id="btnSelecEmp" class="xGrid-tbSearch-btnSearch" title="B�squeda de personal activo..." style="height: 30px; margin-left: 5px; width: 45px;"></a>
                            <div id="dvForm-BusqEmp" title="SISP :: B�squeda de personal activo">
                                <div id="dvGrid-Personal" style="border: none; height: 300px; margin: auto auto; margin-top: 10px; width: 960px;">
                                    <div class="xGrid-dvHeader gradient">
                                        <table class="xGrid-tbSearch">
                                            <tr>
                                                <td>Buscar: <input type="text" name="txtBuscar" size="25" value="" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                                            </tr>
                                        </table>
                                        <table class="xGrid-tbCols">
                                            <tr>  
                                                <th style="width: 5%; text-align: center;">&nbsp;</th>
                                                <th style="width: 30%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                                                <th style="width: 18%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                                                <th style="width: 15%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                                                <th style="width: 25%;" class="xGrid-tbCols-ColSortable">�REA</th>                        
                                                <th style="width: 7%;" class="xGrid-thNo-Class">&nbsp;</th>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="xGrid-dvBody">
                                    
                                    </div>        
                                </div>
                                <input type="hidden" id="hdnUrlDatEmpGrid" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_obt_datos_emp_grid.php');?>" />
                            </div> 
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNombre">Nombre completo:</label></td>
                        <td>
                            <input type="text" name="txtNombre" id="txtNombre" value="" readonly="true" title="" style="width: 300px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCategoria">Categor�a:</label></td>
                        <td>
                            <input type="text" name="txtCategoria" id="txtCategoria" value="" readonly="true" title="" style="width: 300px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtArea">�rea de adscripci�n:</label></td>
                        <td>
                            <input type="text" name="txtArea" id="txtArea" value="" readonly="true" title="" style="width: 450px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="fsetForm-Data">
                <legend>Datos de la baja</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="cbxMotivo">Motivo:</label></td>
                        <td class="validation">
                            <select name="cbxMotivo" id="cbxMotivo" style="max-width: 300px;"> 
                                <option value="0"></option>                               
                                <?php
                                echo $objBajas->AdmcatMotivosBaja->shwMotivosBaja();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaBaja">Fecha de la Baja:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaBaja" id="txtFechaBaja" value="" readonly="true" maxlength="10" title="..." style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumOficio">No. de Oficio:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumOficio" id="txtNumOficio" value="" maxlength="10" title="..." style="width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaOficio">Fecha del Oficio:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaOficio" id="txtFechaOficio" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtObservaciones">Observaciones:</label></td>
                        <td class="validation">
                            <textarea name="txtObservaciones" id="txtObservaciones" style="height: 60px; width: 450px;" ></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
            
            <input type="hidden" name="hdnTypeOper" id="hdnTypeOper" value="1" />
            <input type="hidden" name="hdnUrlSave" id="hdnUrlSave" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_reg_baja.php');?>" />
            <input type="hidden" name="hdnUrlGet" id="hdnUrlGet" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_obt_datos_baja.php');?>" />
            <input type="hidden" name="hdnIdBaja" id="hdnIdBaja" value="0" />
        </form>
    </div>    
    
    <div id="dvForm-Oficio" title="SISP :: Oficios">
        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlOfiPdf" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_obt_ofi_pdf.php');?>" />
    <input type="hidden" id="hdnUrlOfiWord" value="<?php echo $objSys->encrypt('adm/rhumanos/bajas/ajx_obt_ofi_word.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>