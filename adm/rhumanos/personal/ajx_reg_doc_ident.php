<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/admtbl_doctos_ident.class.php';
    $objDocIdent = new AdmtblDoctosIdent();
    
    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Datos Personales
    $objDocIdent->curp = $curp;
    $objDocIdent->foto_frente   = ( !empty($_POST["hdnFotoFte"]) ) ? $curp . '_' . $_POST["hdnFotoFte"] : null;
    $objDocIdent->foto_izq      = ( !empty($_POST["hdnFotoIzq"]) ) ? $curp . '_' . $_POST["hdnFotoIzq"] : null;
    $objDocIdent->foto_der      = ( !empty($_POST["hdnFotoDer"]) ) ? $curp . '_' . $_POST["hdnFotoDer"] : null;
    $objDocIdent->firma         = ( !empty($_POST["hdnFirma"]) ) ? $curp . '_' . $_POST["hdnFirma"] : null;
    $objDocIdent->huella_pulg_izq= ( !empty($_POST["hdnHuellaIzq"]) ) ? $curp . '_' . $_POST["hdnHuellaIzq"] : null;
    $objDocIdent->huella_pulg_der= ( !empty($_POST["hdnHuellaDer"]) ) ? $curp . '_' . $_POST["hdnHuellaDer"] : null;
    
    if( $objDocIdent->chkExist($curp) ){
        $result = $objDocIdent->update();
        $oper = 'Upd';
    } else {
        $result = $objDocIdent->insert();
        $oper = 'Ins';
    }   
    
    if ($result) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['error'] = '';
        
        $path_upload = '../../_uploadfiles/';
        // Se pasan los archivos de im�genes a la carpeta correspondiente
        $path_fotos = '../../expediente/fotos/';       
        if( !empty($objDocIdent->foto_frente) ){
            copy($path_upload . $_POST["hdnFotoFte"], $path_fotos . $objDocIdent->foto_frente);
            unlink($path_upload . $_POST["hdnFotoFte"]);
        }
        if( !empty($objDocIdent->foto_der) ){
            copy($path_upload . $_POST["hdnFotoDer"], $path_fotos . $objDocIdent->foto_der);
            unlink($path_upload . $_POST["hdnFotoDer"]);
        }
        if( !empty($objDocIdent->foto_izq) ){
            copy($path_upload . $_POST["hdnFotoIzq"], $path_fotos . $objDocIdent->foto_izq);
            unlink($path_upload . $_POST["hdnFotoIzq"]);
        }
        // Firma
        $path_firmas = '../../expediente/firmas/';
        if( !empty($objDocIdent->firma) ){
            copy($path_upload . $_POST["hdnFirma"], $path_firmas . $objDocIdent->firma);
            unlink($path_upload . $_POST["hdnFirma"]);
        }
        // Huellas
        $path_huellas = '../../expediente/huellas/';
        if( !empty($objDocIdent->huella_pulg_izq) ){
            copy($path_upload . $_POST["hdnHuellaIzq"], $path_huellas . $objDocIdent->huella_pulg_izq);
            unlink($path_upload . $_POST["hdnHuellaIzq"]);
        }
        if( !empty($objDocIdent->huella_pulg_der) ){
            copy($path_upload . $_POST["hdnHuellaDer"], $path_huellas . $objDocIdent->huella_pulg_der);
            unlink($path_upload . $_POST["hdnHuellaDer"]);
        }
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objDocIdent->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>