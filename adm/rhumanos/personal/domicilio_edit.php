<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admtbl_domicilio.class.php';

$objMunicipio = new AdmcatMunicipios();
$objDomi = new AdmtblDomicilio();
$objDomi->select($_SESSION['xCurp']);

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/domicilio.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('domicilio_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php
                    if( $objDomi->AdmtblDatosPersonales->id_status != 2 ){// diferente de BAJA
                    ?>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificaci�n de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Actualizaci�n : Domicilio</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="txtCalle">Nombre de la Calle:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCalle" id="txtCalle" value="<?php echo $objDomi->calle;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNumExt">No. Exterior:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumExt" id="txtNumExt" value="<?php echo $objDomi->n_exterior;?>" maxlength="5" title="..." style="width: 70px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumInt">No. Interior:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumInt" id="txtNumInt" value="<?php echo $objDomi->n_interior;?>" maxlength="5" title="..." style="width: 70px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtEntreCalle1">Entre la calle:</label></td>
                        <td class="validation">
                            <input type="text" name="txtEntreCalle1" id="txtEntreCalle1" value="<?php echo $objDomi->entre_calle1;?>" maxlength="60" title="..." style="width: 350px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtEntreCalle2">Y la calle:</label></td>
                        <td class="validation">
                            <input type="text" name="txtEntreCalle2" id="txtEntreCalle2" value="<?php echo $objDomi->entre_calle2;?>" maxlength="60" title="..." style="width: 350px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtColonia">Colonia:</label></td>
                        <td class="validation">
                            <input type="text" name="txtColonia" id="txtColonia" value="<?php echo $objDomi->colonia;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCiudad">Ciudad o Localidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCiudad" id="txtCiudad" value="<?php echo $objDomi->ciudad;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCodPostal">C�digo Postal:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCodPostal" id="txtCodPostal" value="<?php echo $objDomi->cod_postal;?>" maxlength="5" title="..." style="width: 70px;" />
                        </td>
                    </tr>   
                    <tr>
                        <td><label for="cbxEntDomicilio">Entidad Federativa:</label></td>
                        <td class="validation">
                            <select name="cbxEntDomicilio" id="cbxEntDomicilio">                                        
                                <?php
                                echo $objMunicipio->AdmcatEntidades->shwEntidades($objDomi->id_entidad, 1);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                         
                    <tr>
                        <td><label for="cbxMpioDomicilio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMpioDomicilio" id="cbxMpioDomicilio" style="max-width: 500px;">                                                                   
                                <?php
                                echo $objMunicipio->shwMunicipios($objDomi->id_municipio, 12); // Entidad: 12=Guerrero
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelFijo">Tel�fono Fijo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelFijo" id="txtTelFijo" value="<?php echo $objDomi->tel_fijo;?>" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelMovil">Tel�fono M�vil:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelMovil" id="txtTelMovil" value="<?php echo $objDomi->tel_movil;?>" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="2" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>