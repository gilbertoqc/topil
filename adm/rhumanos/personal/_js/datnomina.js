$(function(){               
    // Convierte a may�sculas el contenido de los textbox y textarea    
    $('#txtOficinaPag').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
              
    $('#frmRegistro').validate({
        rules:{
            // N�mina
            txtFolioPlaza: 'required',
            cbxTipoPlaza:{ 
                required: true,
                min: 1,
            },
            txtNumEmpleado:{
                required: true,
                digits: true,
            },
            txtSueldo:{ 
                number: true,
            },
            txtNumCuenta:{ 
                number: true,
            }
    	},
    	messages:{    	   
            // Domicilio
            txtFolioPlaza: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxTipoPlaza:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNumEmpleado:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
            },
            txtSueldo:{ 
                number: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
            },
            txtNumCuenta:{ 
                number: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
            }
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    $('#txtFolioPlaza').keyup(function(){
        if( $(this).val().length >= 5 ){
            obtenerDatosPlaza($(this).val());
        } else {
            $('#txtCategPlaza').val('');
            $('#txtCategPlaza').val('');
            $('#hdnIdPlaza').val('0');
        }
    });
    
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a la N�mina del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        //$('#frmRegistro').submit();
                        $(this).dialog('close');
                        guardarDatos();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });
});

function obtenerDatosPlaza(txt){
    $.ajax({
        url: xDcrypt($('#hdnUrlPlaza').val()),
        data: {'txt': txt},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#txtCategPlaza').val('Cargando datos...');
        },
        success: function (xdata) {
            $('#txtCategPlaza').val('');
            if (xdata.rslt){
                if( xdata.id_plaza > 0 && xdata.status == 1 ){
                    $('#txtCategPlaza').val(xdata.categoria);
                    $('#txtAreaPlaza').val(xdata.area);
                    $('#hdnIdPlaza').val(xdata.id_plaza);
                } else if( xdata.id_plaza > 0 && xdata.status == 2 ){
                    shwError('La plaza con el n�mero de folio especificado, ya se encuentra asignada');
                } else {
                    shwError('No se encontr� ninguna plaza con ese folio');
                }
            }
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function guardarDatos(){
    var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Guardando los datos, por favor espere...</div></div>');
    $.ajax({
        url: xDcrypt($('#hdnUrlSave').val()),
        data: $('#frmRegistro').serialize(),
        type: 'post',
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            // D�alogo de espera                     
            frmWait.dialog({
                autoOpen: true,
                modal: true,
                width: 300,
            }); 
        },
        success: function (xdata) {
            //alert(xdata);
            // Cierra el di�logo de espera
            frmWait.dialog('close');            
            if (xdata.rslt){
                $('#dvMsjProcesData').html('<p class="pMsjOk"><span class="spnIconOk"></span>' + xdata.msj + '</p>');
            } else {
                $('#dvMsjProcesData').html('<p class="pMsjError"><span class="spnIconError"></span>' + xdata.error + '</p>');
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}