$(document).ready(function(){   
    // M�scara de los campos tel�fonos
    $('#txtTelFijo').mask('(999) 99-99999');
    $('#txtTelMovil').mask('(999) 99-99999');
            
    // Convierte a may�sculas el contenido de los textbox y textarea    
    $('#txtCalle').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumExt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumInt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle1').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle2').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtColonia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCiudad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
        
    // Control para la carga din�mica de municipios    
    $('#cbxEntDomicilio').change(function(){
        obtenerMunicipios($(this).val());  
    });
        
    $('#frmRegistro').validate({
        rules:{
            // Domicilio
            txtCalle: 'required',
            txtNumExt: 'required',
            txtColonia: 'required',
            txtCiudad: 'required',
            txtCodPostal:{ 
                digits: true,
                minlength: 5,
            },
            cbxEntDomicilio:{
                required: true,
                min: 1,
            },
            cbxMpioDomicilio:{
                required: true,
                min: 1,
            },            
    	},
    	messages:{    	   
            // Domicilio
            txtCalle: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumExt: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtColonia: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCiudad: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCodPostal:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o del C�digo Postal debe de ser 5 digitos"></span>',
            },
            cbxEntDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
        
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados al Domicilio del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });
});

function obtenerMunicipios(id){
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'filtro': 1},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxMpioDomicilio').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxMpioDomicilio').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}