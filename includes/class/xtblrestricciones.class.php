<?php
/**
 *
 */
class Xtblrestricciones
{
    public $id_restriccion; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tabla; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $campo; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $tipo; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $operador; /** @Tipo: enum('=','!=','<','>','<=','>=','IN','NOT IN'), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $valor; /** @Tipo: varchar(250), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xtblusuarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xtblusuarios.class.php';
        $this->Xtblusuarios = new Xtblusuarios();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_restriccion)
    {
        $sql = "SELECT id_restriccion, descripcion, tabla, campo, operador, valor, id_usuario
                FROM xtblrestricciones
                WHERE id_restriccion=:id_restriccion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_restriccion' => $id_restriccion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_restriccion = $data['id_restriccion'];
            $this->descripcion = $data['descripcion'];
            $this->tabla = $data['tabla'];
            $this->campo = $data['campo'];
            $this->tipo = $data['tipo'];
            $this->operador = $data['operador'];
            $this->valor = $data['valor'];
            $this->id_usuario = $data['id_usuario'];

            $this->Xtblusuarios->select($this->id_usuario);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectUser($sqlWhere='', $sqlOrder="")
    {
        $sql = "SELECT a.id_restriccion, a.descripcion, a.tabla, a.campo, a.tipo, a.operador, a.valor, a.id_usuario 
                FROM xtblrestricciones a 
                 ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";        
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_restriccion' => $data['id_restriccion'],
                               'descripcion' => $data['descripcion'],
                               'tabla' => $data['tabla'],
                               'campo' => $data['campo'],
                               'tipo' => $data['tipo'],
                               'operador' => $data['operador'],
                               'valor' => $data['valor'],
                               'id_usuario' => $data['id_usuario'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_restriccion, a.descripcion, a.tabla, a.campo, a.tipo, a.operador, a.valor, a.id_usuario,
                  b.id_usuario, b.nom_usr, b.pswd, b.nombre, b.id_perfil, b.filtro_ip, b.fecha_reg, b.fecha_edit, b.fecha_expira, b.ultimo_acceso, b.foto, b.stat
                FROM xtblrestricciones a 
                 LEFT JOIN xtblusuarios b ON a.id_usuario=b.id_usuario";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_restriccion' => $data['id_restriccion'],
                               'descripcion' => $data['descripcion'],
                               'tabla' => $data['tabla'],
                               'campo' => $data['campo'],
                               'tipo' => $data['tipo'],
                               'operador' => $data['operador'],
                               'valor' => $data['valor'],
                               'id_usuario' => $data['id_usuario'],
                               'xtblusuarios_nom_usr' => $data['nom_usr'],
                               'xtblusuarios_pswd' => $data['pswd'],
                               'xtblusuarios_nombre' => $data['nombre'],
                               'xtblusuarios_id_perfil' => $data['id_perfil'],
                               'xtblusuarios_filtro_ip' => $data['filtro_ip'],
                               'xtblusuarios_fecha_reg' => $data['fecha_reg'],
                               'xtblusuarios_fecha_edit' => $data['fecha_edit'],
                               'xtblusuarios_fecha_expira' => $data['fecha_expira'],
                               'xtblusuarios_ultimo_acceso' => $data['ultimo_acceso'],
                               'xtblusuarios_foto' => $data['foto'],
                               'xtblusuarios_stat' => $data['stat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtblrestricciones(id_restriccion, descripcion, tabla, campo, tipo, operador, valor, id_usuario)
                VALUES(:id_restriccion, :descripcion, :tabla, :campo, :tipo, :operador, :valor, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_restriccion" => $this->id_restriccion, ":descripcion" => $this->descripcion, ":tabla" => $this->tabla, ":campo" => $this->campo, ":tipo" => $this->tipo, ":operador" => $this->operador, ":valor" => $this->valor, ":id_usuario" => $this->id_usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtblrestricciones
                   SET descripcion=:descripcion, tabla=:tabla, campo=:campo, tipo=:tipo, operador=:operador, valor=:valor, id_usuario=:id_usuario
                WHERE id_restriccion=:id_restriccion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_restriccion" => $this->id_restriccion, ":descripcion" => $this->descripcion, ":tabla" => $this->tabla, ":campo" => $this->campo, ":tipo" => $this->tipo, ":operador" => $this->operador, ":valor" => $this->valor, ":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {
        $sql = "DELETE FROM xtblrestricciones WHERE id_restriccion=:id_restriccion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_restriccion" => $this->id_restriccion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /*
     * Funciones para obtener las tablas, campos y dem�s par�metros configurables
     * para generar restricciones
     */
    public function getTables()
    {
        //-- Obtiene el listado de las tablas de la base de datos...
        $qry = $this->_conexBD->prepare("SHOW TABLES;");
        $qry->execute();
        //-- Recorrido del listado de tablas...
        $tables = array();        
        while( $data = $qry->fetch() ){
            $table = $data[0];
            if( !preg_match("/cat_/i", $table) && !preg_match("/^x/i", $table) )
                $tables[] = $table;
        }
        
        return $tables;
    }

    public function getFields($table)
    {
        //-- Obtiene el listado de los campos de la tabla recibida por par�metro...
        $qry = $this->_conexBD->prepare("SHOW COLUMNS FROM $table;");
        $qry->execute();
        //-- Recorrido del listado de campos...
        $fields = array();
        while( $data = $qry->fetchObject() ){
            $type = explode('(', $data->Type);
            $fields[] = array('campo' => $data->Field, 'tipo' => $type[0]);
        }
        
        return $fields;
    }

    public function getOperators()
    {
        $qry = $this->_conexBD->prepare("SHOW COLUMNS FROM xtblrestricciones WHERE Field = 'operador';");
        $qry->execute();
        $type = $qry->fetchObject()->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }


}


?>