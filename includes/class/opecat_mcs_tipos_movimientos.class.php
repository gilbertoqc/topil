<?php
/**
 *
 */
class OpecatMcsTiposMovimientos
{
    public $id_tipo_movimiento; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_movimiento; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

     /**
     * Funci�n para mostrar la lista de tipos de movimientos dentro de un combobox.
     * @param int $id, id del tipo de movimiento seleccionado por deafult
     * @return array html(options)
     */
    public function getTiposMovimientos($id=0)
    {
        $aryDatos = $this->selectAll('id_tipo_movimiento Asc','');
        $html = '<option value="0">- Seleccionado -</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_movimiento"] )
                $html .= '<option value="'.$datos["id_tipo_movimiento"].'" selected>'.$datos["tipo_movimiento"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_movimiento"].'" >'.$datos["tipo_movimiento"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_movimiento)
    {
        $sql = "SELECT id_tipo_movimiento, tipo_movimiento
                FROM opecat_mcs_tipos_movimientos
                WHERE id_tipo_movimiento=:id_tipo_movimiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_movimiento' => $id_tipo_movimiento));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_movimiento = $data['id_tipo_movimiento'];
            $this->tipo_movimiento = $data['tipo_movimiento'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_tipo_movimiento, a.tipo_movimiento
                FROM opecat_mcs_tipos_movimientos a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_movimiento' => $data['id_tipo_movimiento'],
                               'tipo_movimiento' => $data['tipo_movimiento'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mcs_tipos_movimientos(id_tipo_movimiento, tipo_movimiento)
                VALUES(:id_tipo_movimiento, :tipo_movimiento);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_movimiento" => $this->id_tipo_movimiento, ":tipo_movimiento" => $this->tipo_movimiento));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mcs_tipos_movimientos
                   SET tipo_movimiento=:tipo_movimiento
                WHERE id_tipo_movimiento=:id_tipo_movimiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_movimiento" => $this->id_tipo_movimiento, ":tipo_movimiento" => $this->tipo_movimiento));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>