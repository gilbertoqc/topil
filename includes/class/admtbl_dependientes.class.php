<?php
/**
 *
 */
class AdmtblDependientes
{
    public $id_dependiente; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $nombre; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_nac; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_parentesco; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    //public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatParentescos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        //require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_parentescos.class.php';
        //$this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatParentescos = new AdmcatParentescos();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_dependiente)
    {
        $sql = "SELECT id_dependiente, curp, nombre, a_paterno, a_materno, fecha_nac, id_parentesco
                FROM admtbl_dependientes
                WHERE id_dependiente=:id_dependiente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_dependiente' => $id_dependiente));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_dependiente = $data['id_dependiente'];
            $this->curp = $data['curp'];
            $this->nombre = $data['nombre'];
            $this->a_paterno = $data['a_paterno'];
            $this->a_materno = $data['a_materno'];
            $this->fecha_nac = $data['fecha_nac'];
            $this->id_parentesco = $data['id_parentesco'];

            //$this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatParentescos->select($this->id_parentesco);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_dependiente, a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.id_parentesco,
                  b.parentesco
                FROM admtbl_dependientes a 
                 LEFT JOIN admcat_parentescos b ON a.id_parentesco=b.id_parentesco";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_dependiente' => $data['id_dependiente'],
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'fecha_nac' => $data['fecha_nac'],
                               'id_parentesco' => $data['id_parentesco'],
                               'parentesco' => $data['parentesco'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_dependientes(id_dependiente, curp, nombre, a_paterno, a_materno, fecha_nac, id_parentesco)
                VALUES(:id_dependiente, :curp, :nombre, :a_paterno, :a_materno, :fecha_nac, :id_parentesco);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependiente" => $this->id_dependiente, ":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":id_parentesco" => $this->id_parentesco));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_dependientes
                   SET curp=:curp, nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, fecha_nac=:fecha_nac, id_parentesco=:id_parentesco
                WHERE id_dependiente=:id_dependiente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependiente" => $this->id_dependiente, ":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":id_parentesco" => $this->id_parentesco));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>