<?php
/**
 *
 */
class LogtblVehObservaciones
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblVehDetalles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_veh_detalles.class.php';
        $this->LogtblVehDetalles = new LogtblVehDetalles();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo)
    {
        $sql = "SELECT id_vehiculo, observaciones
                FROM logtbl_veh_observaciones
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->observaciones = $data['observaciones'];

            $this->LogtblVehDetalles->select($this->id_vehiculo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.observaciones,
                  b.id_vehiculo, b.kilometraje, b.espejo_der, b.espejo_izq, b.espejo_estado, b.cuarto_der, b.cuarto_izq, b.cuarto_estado, b.fanal_der, b.fanal_izq, b.fanal_estado, b.calavera_der, b.calavera_izq, b.calavera_estado, b.moldura_der, b.moldura_izq, b.moldura_del, b.moldura_tras, b.emblema_der, b.emblema_izq, b.emblema_del, b.emblema_tras, b.pintura, b.faros_antiniebla, b.limpiadores, b.limpiadores_estado, b.espejo_interior, b.espejo_interior_estado, b.encendedor, b.cinturon, b.tapetes, b.disco_compacto, b.antena, b.manijas, b.cenicero, b.claxon, b.redio_amfm, b.vestiduras, b.otros, b.tapones, b.rines_acero, b.torreta, b.torreta_estado, b.radio_movil, b.radio_movil_estado, b.alto_parlante, b.alto_parlante_estado, b.tumbaburros, b.lona, b.rin_aluminio, b.aa, b.roll_bar, b.redillas, b.llave_espanola, b.llave_allen, b.desarmador_plano, b.desarmador_cruz, b.extintor, b.llave_bujia, b.llantas_cantidad, b.llantas_medida, b.id_marca_llantas, b.llantas_vida_util
                FROM logtbl_veh_observaciones a 
                 LEFT JOIN logtbl_veh_detalles b ON a.id_vehiculo=b.id_vehiculo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'observaciones' => $data['observaciones'],
                               'logtbl_veh_detalles_kilometraje' => $data['kilometraje'],
                               'logtbl_veh_detalles_espejo_der' => $data['espejo_der'],
                               'logtbl_veh_detalles_espejo_izq' => $data['espejo_izq'],
                               'logtbl_veh_detalles_espejo_estado' => $data['espejo_estado'],
                               'logtbl_veh_detalles_cuarto_der' => $data['cuarto_der'],
                               'logtbl_veh_detalles_cuarto_izq' => $data['cuarto_izq'],
                               'logtbl_veh_detalles_cuarto_estado' => $data['cuarto_estado'],
                               'logtbl_veh_detalles_fanal_der' => $data['fanal_der'],
                               'logtbl_veh_detalles_fanal_izq' => $data['fanal_izq'],
                               'logtbl_veh_detalles_fanal_estado' => $data['fanal_estado'],
                               'logtbl_veh_detalles_calavera_der' => $data['calavera_der'],
                               'logtbl_veh_detalles_calavera_izq' => $data['calavera_izq'],
                               'logtbl_veh_detalles_calavera_estado' => $data['calavera_estado'],
                               'logtbl_veh_detalles_moldura_der' => $data['moldura_der'],
                               'logtbl_veh_detalles_moldura_izq' => $data['moldura_izq'],
                               'logtbl_veh_detalles_moldura_del' => $data['moldura_del'],
                               'logtbl_veh_detalles_moldura_tras' => $data['moldura_tras'],
                               'logtbl_veh_detalles_emblema_der' => $data['emblema_der'],
                               'logtbl_veh_detalles_emblema_izq' => $data['emblema_izq'],
                               'logtbl_veh_detalles_emblema_del' => $data['emblema_del'],
                               'logtbl_veh_detalles_emblema_tras' => $data['emblema_tras'],
                               'logtbl_veh_detalles_pintura' => $data['pintura'],
                               'logtbl_veh_detalles_faros_antiniebla' => $data['faros_antiniebla'],
                               'logtbl_veh_detalles_limpiadores' => $data['limpiadores'],
                               'logtbl_veh_detalles_limpiadores_estado' => $data['limpiadores_estado'],
                               'logtbl_veh_detalles_espejo_interior' => $data['espejo_interior'],
                               'logtbl_veh_detalles_espejo_interior_estado' => $data['espejo_interior_estado'],
                               'logtbl_veh_detalles_encendedor' => $data['encendedor'],
                               'logtbl_veh_detalles_cinturon' => $data['cinturon'],
                               'logtbl_veh_detalles_tapetes' => $data['tapetes'],
                               'logtbl_veh_detalles_disco_compacto' => $data['disco_compacto'],
                               'logtbl_veh_detalles_antena' => $data['antena'],
                               'logtbl_veh_detalles_manijas' => $data['manijas'],
                               'logtbl_veh_detalles_cenicero' => $data['cenicero'],
                               'logtbl_veh_detalles_claxon' => $data['claxon'],
                               'logtbl_veh_detalles_redio_amfm' => $data['redio_amfm'],
                               'logtbl_veh_detalles_vestiduras' => $data['vestiduras'],
                               'logtbl_veh_detalles_otros' => $data['otros'],
                               'logtbl_veh_detalles_tapones' => $data['tapones'],
                               'logtbl_veh_detalles_rines_acero' => $data['rines_acero'],
                               'logtbl_veh_detalles_torreta' => $data['torreta'],
                               'logtbl_veh_detalles_torreta_estado' => $data['torreta_estado'],
                               'logtbl_veh_detalles_radio_movil' => $data['radio_movil'],
                               'logtbl_veh_detalles_radio_movil_estado' => $data['radio_movil_estado'],
                               'logtbl_veh_detalles_alto_parlante' => $data['alto_parlante'],
                               'logtbl_veh_detalles_alto_parlante_estado' => $data['alto_parlante_estado'],
                               'logtbl_veh_detalles_tumbaburros' => $data['tumbaburros'],
                               'logtbl_veh_detalles_lona' => $data['lona'],
                               'logtbl_veh_detalles_rin_aluminio' => $data['rin_aluminio'],
                               'logtbl_veh_detalles_aa' => $data['aa'],
                               'logtbl_veh_detalles_roll_bar' => $data['roll_bar'],
                               'logtbl_veh_detalles_redillas' => $data['redillas'],
                               'logtbl_veh_detalles_llave_espanola' => $data['llave_espanola'],
                               'logtbl_veh_detalles_llave_allen' => $data['llave_allen'],
                               'logtbl_veh_detalles_desarmador_plano' => $data['desarmador_plano'],
                               'logtbl_veh_detalles_desarmador_cruz' => $data['desarmador_cruz'],
                               'logtbl_veh_detalles_extintor' => $data['extintor'],
                               'logtbl_veh_detalles_llave_bujia' => $data['llave_bujia'],
                               'logtbl_veh_detalles_llantas_cantidad' => $data['llantas_cantidad'],
                               'logtbl_veh_detalles_llantas_medida' => $data['llantas_medida'],
                               'logtbl_veh_detalles_id_marca_llantas' => $data['id_marca_llantas'],
                               'logtbl_veh_detalles_llantas_vida_util' => $data['llantas_vida_util'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_observaciones(id_vehiculo, observaciones)
                VALUES(:id_vehiculo, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_observaciones
                   SET observaciones=:observaciones
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>