<?php
/**
 *
 */
class OpecatMidUnidadesGeneralidades
{
    public $id_unidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $unidad; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos    

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de unidades en el combobox
     * @param int $id, id del regiones seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_Unidades_Generalidades( $id_generalidad, $id_unidad ){
        if( ( $id_generalidad != 0 )  || ( $id_generalidad != '') ){
            $aryDatos = $this->selectAll('b.id_generalidad = ' . $id_generalidad, ' a.id_unidad asc ');
        }else{
            $aryDatos = $this->selectAll('', ' a.id_unidad asc ');
        }
            
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_unidad == $datos["id_unidad"] )
                $html .= '<option value="'.$datos["id_unidad"].'" selected>'.$datos["unidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_unidad"].'" >'.$datos["unidad"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_unidad)
    {
        $sql = "SELECT id_unidad, unidad
                FROM opecat_mid_unidades_generalidades
                WHERE id_unidad=:id_unidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_unidad' => $id_unidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_unidad = $data['id_unidad'];
            $this->unidad = $data['unidad'];
            
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_unidad, a.unidad                  
                FROM opecat_mid_unidades_generalidades a
                LEFT JOIN opecat_mid_generalidades_unidades b On b.id_unidad=a.id_unidad";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_unidad' => $data['id_unidad'],
                               'unidad' => $data['unidad'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_unidades_generalidades(id_unidad, unidad)
                VALUES(:id_unidad, :unidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_unidad" => $this->id_unidad, ":unidad" => $this->unidad));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_unidades_generalidades
                   SET unidad=:unidad
                WHERE id_unidad=:id_unidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_unidad" => $this->id_unidad, ":unidad" => $this->unidad));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>