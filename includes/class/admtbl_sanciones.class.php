<?php
/**
 *
 */
class AdmtblSanciones
{
    public $id_sancion; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_motivo_sancion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_sancion; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $duracion; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMotivoSancion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoSancion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_motivo_sancion.class.php';
        require_once 'admcat_tipo_sancion.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatMotivoSancion = new AdmcatMotivoSancion();
        $this->AdmcatTipoSancion = new AdmcatTipoSancion();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_sancion)
    {
        $sql = "SELECT id_sancion, curp, id_motivo_sancion, id_tipo_sancion, fecha_reg, duracion, observaciones
                FROM admtbl_sanciones
                WHERE id_sancion=:id_sancion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_sancion' => $id_sancion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_sancion = $data['id_sancion'];
            $this->curp = $data['curp'];
            $this->id_motivo_sancion = $data['id_motivo_sancion'];
            $this->id_tipo_sancion = $data['id_tipo_sancion'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->duracion = $data['duracion'];
            $this->observaciones = $data['observaciones'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatMotivoSancion->select($this->id_motivo_sancion);
            $this->AdmcatTipoSancion->select($this->id_tipo_sancion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_sancion, a.curp, a.id_motivo_sancion, a.id_tipo_sancion, a.fecha_reg, a.duracion, a.observaciones,
                  b.motivo_sancion, 
                  c.tipo_sancion
                FROM admtbl_sanciones a 
                 LEFT JOIN admcat_motivo_sancion b ON a.id_motivo_sancion=b.id_motivo_sancion
                 LEFT JOIN admcat_tipo_sancion c ON a.id_tipo_sancion=c.id_tipo_sancion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_sancion' => $data['id_sancion'],
                               'curp' => $data['curp'],
                               'id_motivo_sancion' => $data['id_motivo_sancion'],
                               'id_tipo_sancion' => $data['id_tipo_sancion'],
                               'fecha_reg' => $data['fecha_reg'],
                               'duracion' => $data['duracion'],
                               'observaciones' => $data['observaciones'],                               
                               'motivo_sancion' => $data['motivo_sancion'],                               
                               'tipo_sancion' => $data['tipo_sancion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_sanciones(id_sancion, curp, id_motivo_sancion, id_tipo_sancion, fecha_reg, duracion, observaciones)
                VALUES(:id_sancion, :curp, :id_motivo_sancion, :id_tipo_sancion, :fecha_reg, :duracion, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_sancion" => $this->id_sancion, ":curp" => $this->curp, ":id_motivo_sancion" => $this->id_motivo_sancion, ":id_tipo_sancion" => $this->id_tipo_sancion, ":fecha_reg" => $this->fecha_reg, ":duracion" => $this->duracion, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_sanciones
                   SET curp=:curp, id_motivo_sancion=:id_motivo_sancion, id_tipo_sancion=:id_tipo_sancion, fecha_reg=:fecha_reg, duracion=:duracion, observaciones=:observaciones
                WHERE id_sancion=:id_sancion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_sancion" => $this->id_sancion, ":curp" => $this->curp, ":id_motivo_sancion" => $this->id_motivo_sancion, ":id_tipo_sancion" => $this->id_tipo_sancion, ":fecha_reg" => $this->fecha_reg, ":duracion" => $this->duracion, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>