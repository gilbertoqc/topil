<?php
/**
 *
 */
class Xcatpermisos
{
    public $id_permiso; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $permiso; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(35), @Acepta Nulos: SI, @Llave: --, @Default: NULL */
    public $tipo; /** @Tipo: enum('General','Especial'), @Acepta Nulos: NO, @Llave: --, @Default: General */
    public $stat; /** @Tipo: TINYINT(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    
    public $msjError; // almacena el mensaje de error si éste ocurre
    private $_conexBD; // objeto de conexión a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Función para mostrar la lista de permisos especiales disponibles dentro de un combobox.
     * @param int $id, id del permiso seleccionado por deafult 
     * @return array html(options)
     */
    public function shwPermisos($id=0, $sqlWhere=''){
        $aryDatos = $this->selectAll($sqlWhere, 'permiso Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_permiso"] )
                $html .= '<option value="'.$datos["id_permiso"].'" data-descrip="'.$datos['descripcion'].'" selected>'.$datos["permiso"].'</option>';
            else
                $html .= '<option value="'.$datos["id_permiso"].'" data-descrip="'.$datos['descripcion'].'">'.$datos["permiso"].'</option>';
        }
        return $html;
    }

    /**
     * Función para obtener un registro específico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realizó con éxito
     */
    public function select($id_permiso)
    {
        $sql = "SELECT id_permiso, permiso, descripcion, tipo, stat
                FROM xcatpermisos
                WHERE id_permiso=:id_permiso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_permiso' => $id_permiso));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_permiso = $data['id_permiso'];
            $this->permiso = $data['permiso'];
            $this->descripcion = $data['descripcion'];
            $this->tipo = $data['tipo'];
            $this->stat = $data['stat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selección de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_permiso, a.permiso, a.descripcion, a.tipo, a.stat
                FROM xcatpermisos a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_permiso' => $data['id_permiso'],
                               'permiso' => $data['permiso'],
                               'descripcion' => $data['descripcion'],
                               'tipo' => $data['tipo'],
                               'stat' => $data['stat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el último id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xcatpermisos(id_permiso, permiso, descripcion, tipo, stat)
                VALUES(:id_permiso, :permiso, :descripcion, :tipo, :stat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permiso" => $this->id_permiso, ":permiso" => $this->permiso, ":descripcion" => $this->descripcion, ":tipo" => $this->tipo, ":stat" => $this->stat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xcatpermisos
                   SET permiso=:permiso, descripcion=:descripcion, tipo=:tipo, stat=:stat
                WHERE id_permiso=:id_permiso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permiso" => $this->id_permiso, ":permiso" => $this->permiso, ":descripcion" => $this->descripcion, ":tipo" => $this->tipo, ":stat" => $this->stat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }

}


?>