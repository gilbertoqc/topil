<?php
/**
 *
 */
class OpecatMidMunicipios
{
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_estado; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $municipio; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $alcalde; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidEstados; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_estados.class.php';
        $this->OpecatMidEstados = new OpecatMidEstados();
    }

    /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Municipios($id=0){
        $aryDatos = $this->selectAll('id_municipio Asc','');		
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_municipio"] )
                $html .= '<option value="'.$datos["id_municipio"].'" selected>'.$datos["municipio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_municipio"].'" >'.$datos["municipio"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_municipio)
    {
        $sql = "SELECT id_municipio, id_estado, municipio, alcalde
                FROM opecat_mid_municipios
                WHERE id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_municipio' => $id_municipio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_municipio = $data['id_municipio'];
            $this->id_estado = $data['id_estado'];
            $this->municipio = $data['municipio'];
            $this->alcalde = $data['alcalde'];

            $this->OpecatMidEstados->select($this->id_estado);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_municipio, a.id_estado, a.municipio, a.alcalde,
                  b.id_estado, b.estado
                FROM opecat_mid_municipios a
                 LEFT JOIN opecat_mid_estados b ON a.id_estado=b.id_estado
				 WHERE b.id_estado='12'";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_municipio' => $data['id_municipio'],
                               'id_estado' => $data['id_estado'],
                               'municipio' => $data['municipio'],
                               'alcalde' => $data['alcalde'],
                               'opecat_mid_estados_estado' => $data['estado'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_municipios(id_municipio, id_estado, municipio, alcalde)
                VALUES(:id_municipio, :id_estado, :municipio, :alcalde);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_estado" => $this->id_estado, ":municipio" => $this->municipio, ":alcalde" => $this->alcalde));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_municipios
                   SET id_estado=:id_estado, municipio=:municipio, alcalde=:alcalde
                WHERE id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_estado" => $this->id_estado, ":municipio" => $this->municipio, ":alcalde" => $this->alcalde));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>