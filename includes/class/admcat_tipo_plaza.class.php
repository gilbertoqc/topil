<?php
/**
 *
 */
class AdmcatTipoPlaza
{
    public $id_tipo_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_plaza; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de los tipos de plaza dentro de un combobox.
     * @param int $id, id del tipo de plaza seleccionado por deafult     
     * @return array html(options)
     */
    public function shwTiposPlaza($id=0){
        $aryDatos = $this->selectAll('', 'tipo_plaza Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_plaza"] )
                $html .= '<option value="'.$datos["id_tipo_plaza"].'" selected>'.$datos["tipo_plaza"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_plaza"].'" >'.$datos["tipo_plaza"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_plaza)
    {
        $sql = "SELECT id_tipo_plaza, tipo_plaza
                FROM admcat_tipo_plaza
                WHERE id_tipo_plaza=:id_tipo_plaza;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_plaza' => $id_tipo_plaza));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_plaza = $data['id_tipo_plaza'];
            $this->tipo_plaza = $data['tipo_plaza'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_plaza, a.tipo_plaza
                FROM admcat_tipo_plaza a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_plaza' => $data['id_tipo_plaza'],
                               'tipo_plaza' => $data['tipo_plaza'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_tipo_plaza(id_tipo_plaza, tipo_plaza)
                VALUES(:id_tipo_plaza, :tipo_plaza);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_plaza" => $this->id_tipo_plaza, ":tipo_plaza" => $this->tipo_plaza));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_tipo_plaza
                   SET tipo_plaza=:tipo_plaza
                WHERE id_tipo_plaza=:id_tipo_plaza;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_plaza" => $this->id_tipo_plaza, ":tipo_plaza" => $this->tipo_plaza));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>