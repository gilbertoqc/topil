<?php
/**
 *
 */
class OpecatMidPersonasRolesCausas
{
    public $id_persona_rol; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_persona_causa; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidPersonasRoles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidPersonasCausas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_personas_roles.class.php';
        require_once 'opecat_mid_personas_causas.class.php';
        $this->OpecatMidPersonasRoles = new OpecatMidPersonasRoles();
        $this->OpecatMidPersonasCausas = new OpecatMidPersonasCausas();
    }

    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Personas_Roles_Causas($id=0, $id_rol=0){
        $sql_where = ( $id_rol > 0 ) ? "a.id_persona_rol=" . $id_rol : "";
        $aryDatos = $this->selectAll($sql_where,'id_persona_rol Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_persona_causa"] )
                $html .= '<option value="'.$datos["id_persona_causa"].'" selected>'.$datos["persona_causa"].'</option>';
            else
                $html .= '<option value="'.$datos["id_persona_causa"].'" >'.$datos["persona_causa"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_persona_rol, $id_persona_causa)
    {
        $sql = "SELECT id_persona_rol, id_persona_causa
                FROM opecat_mid_personas_roles_causas
                WHERE id_persona_rol=:id_persona_rol AND id_persona_causa=:id_persona_causa;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_persona_rol' => $id_persona_rol, ':id_persona_causa' => $id_persona_causa));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_persona_rol = $data['id_persona_rol'];
            $this->id_persona_causa = $data['id_persona_causa'];

            $this->OpecatMidPersonasRoles->select($this->id_persona_rol);
            $this->OpecatMidPersonasCausas->select($this->id_persona_causa);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere="", $sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_persona_rol, a.id_persona_causa,
                  b.persona_rol,
                  c.persona_causa
                FROM opecat_mid_personas_roles_causas a
                  LEFT JOIN opecat_mid_personas_roles b ON a.id_persona_rol=b.id_persona_rol
                  LEFT JOIN opecat_mid_personas_causas c ON a.id_persona_causa=c.id_persona_causa";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_persona_rol' => $data['id_persona_rol'],
                               'id_persona_causa' => $data['id_persona_causa'],
                               'persona_rol' => $data['persona_rol'],
                               'persona_causa' => $data['persona_causa'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_personas_roles_causas(id_persona_rol, id_persona_causa)
                VALUES(:id_persona_rol, :id_persona_causa);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_persona_rol" => $this->id_persona_rol, ":id_persona_causa" => $this->id_persona_causa));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_personas_roles_causas
                   SET
                WHERE id_persona_rol=:id_persona_rol AND id_persona_causa=:id_persona_causa;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_persona_rol" => $this->id_persona_rol, ":id_persona_causa" => $this->id_persona_causa));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>