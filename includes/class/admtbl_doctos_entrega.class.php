<?php
/**
 *
 */
class AdmtblDoctosEntrega
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $solicitud_empleo; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $curriculum; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $acta_nac; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $curp_docto; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $credencial; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $fotos; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $cartilla_militar; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $carta_recomend; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $cert_medico; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $const_no_inhab; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $const_no_penal; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */
    public $croquis; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, solicitud_empleo, curriculum, acta_nac, curp_docto, credencial, fotos, cartilla_militar, carta_recomend, cert_medico, const_no_inhab, const_no_penal, croquis
                FROM admtbl_doctos_entrega
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->solicitud_empleo = $data['solicitud_empleo'];
            $this->curriculum = $data['curriculum'];
            $this->acta_nac = $data['acta_nac'];
            $this->curp_docto = $data['curp_docto'];
            $this->credencial = $data['credencial'];
            $this->fotos = $data['fotos'];
            $this->cartilla_militar = $data['cartilla_militar'];
            $this->carta_recomend = $data['carta_recomend'];
            $this->cert_medico = $data['cert_medico'];
            $this->const_no_inhab = $data['const_no_inhab'];
            $this->const_no_penal = $data['const_no_penal'];
            $this->croquis = $data['croquis'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.solicitud_empleo, a.curriculum, a.acta_nac, a.curp_docto, a.credencial, a.fotos, a.cartilla_militar, a.carta_recomend, a.cert_medico, a.const_no_inhab, a.const_no_penal, a.croquis,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status
                FROM admtbl_doctos_entrega a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'solicitud_empleo' => $data['solicitud_empleo'],
                               'curriculum' => $data['curriculum'],
                               'acta_nac' => $data['acta_nac'],
                               'curp_docto' => $data['curp_docto'],
                               'credencial' => $data['credencial'],
                               'fotos' => $data['fotos'],
                               'cartilla_militar' => $data['cartilla_militar'],
                               'carta_recomend' => $data['carta_recomend'],
                               'cert_medico' => $data['cert_medico'],
                               'const_no_inhab' => $data['const_no_inhab'],
                               'const_no_penal' => $data['const_no_penal'],
                               'croquis' => $data['croquis'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_doctos_entrega(curp, solicitud_empleo, curriculum, acta_nac, curp_docto, credencial, fotos, cartilla_militar, carta_recomend, cert_medico, const_no_inhab, const_no_penal, croquis)
                VALUES(:curp, :solicitud_empleo, :curriculum, :acta_nac, :curp_docto, :credencial, :fotos, :cartilla_militar, :carta_recomend, :cert_medico, :const_no_inhab, :const_no_penal, :croquis);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":solicitud_empleo" => $this->solicitud_empleo, ":curriculum" => $this->curriculum, ":acta_nac" => $this->acta_nac, ":curp_docto" => $this->curp_docto, ":credencial" => $this->credencial, ":fotos" => $this->fotos, ":cartilla_militar" => $this->cartilla_militar, ":carta_recomend" => $this->carta_recomend, ":cert_medico" => $this->cert_medico, ":const_no_inhab" => $this->const_no_inhab, ":const_no_penal" => $this->const_no_penal, ":croquis" => $this->croquis));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_doctos_entrega
                   SET solicitud_empleo=:solicitud_empleo, curriculum=:curriculum, acta_nac=:acta_nac, curp_docto=:curp_docto, credencial=:credencial, fotos=:fotos, cartilla_militar=:cartilla_militar, carta_recomend=:carta_recomend, cert_medico=:cert_medico, const_no_inhab=:const_no_inhab, const_no_penal=:const_no_penal, croquis=:croquis
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":solicitud_empleo" => $this->solicitud_empleo, ":curriculum" => $this->curriculum, ":acta_nac" => $this->acta_nac, ":curp_docto" => $this->curp_docto, ":credencial" => $this->credencial, ":fotos" => $this->fotos, ":cartilla_militar" => $this->cartilla_militar, ":carta_recomend" => $this->carta_recomend, ":cert_medico" => $this->cert_medico, ":const_no_inhab" => $this->const_no_inhab, ":const_no_penal" => $this->const_no_penal, ":croquis" => $this->croquis));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>