<?php
/**
 *
 */
class OpecatMidTiposCuarteles
{
    public $id_tipo_cuartel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_cuartel; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidTiposCuarteles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_tipos_cuarteles.class.php';
        $this->OpecatMidTiposCuarteles = new OpecatMidTiposCuarteles();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_cuartel)
    {
        $sql = "SELECT id_tipo_cuartel, tipo_cuartel
                FROM opecat_mid_tipos_cuarteles
                WHERE id_tipo_cuartel=:id_tipo_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_cuartel' => $id_tipo_cuartel));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_cuartel = $data['id_tipo_cuartel'];
            $this->tipo_cuartel = $data['tipo_cuartel'];

            $this->OpecatMidTiposCuarteles->select($this->id_tipo_cuartel);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_tipo_cuartel, a.tipo_cuartel,
                  b.id_tipo_cuartel, b.tipo_cuartel
                FROM opecat_mid_tipos_cuarteles a 
                 LEFT JOIN opecat_mid_tipos_cuarteles b ON a.id_tipo_cuartel=b.id_tipo_cuartel";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_cuartel' => $data['id_tipo_cuartel'],
                               'tipo_cuartel' => $data['tipo_cuartel'],
                               'opecat_mid_tipos_cuarteles_tipo_cuartel' => $data['tipo_cuartel'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_tipos_cuarteles(id_tipo_cuartel, tipo_cuartel)
                VALUES(:id_tipo_cuartel, :tipo_cuartel);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_cuartel" => $this->id_tipo_cuartel, ":tipo_cuartel" => $this->tipo_cuartel));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_tipos_cuarteles
                   SET tipo_cuartel=:tipo_cuartel
                WHERE id_tipo_cuartel=:id_tipo_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_cuartel" => $this->id_tipo_cuartel, ":tipo_cuartel" => $this->tipo_cuartel));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>