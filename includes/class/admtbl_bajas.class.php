<?php
/**
 *
 */
class AdmtblBajas
{
    public $id_baja; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_baja; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $num_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_motivo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblPlantillaPlazas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMotivosBaja; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admtbl_plantilla_plazas.class.php';
        require_once 'admcat_motivos_baja.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmtblPlantillaPlazas = new AdmtblPlantillaPlazas();
        $this->AdmcatMotivosBaja = new AdmcatMotivosBaja();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_baja)
    {
        $sql = "SELECT id_baja, curp, id_plaza, fecha_baja, num_oficio, fecha_oficio, observaciones, id_motivo
                FROM admtbl_bajas
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_baja' => $id_baja));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_baja = $data['id_baja'];
            $this->curp = $data['curp'];
            $this->id_plaza = $data['id_plaza'];
            $this->fecha_baja = $data['fecha_baja'];
            $this->num_oficio = $data['num_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->observaciones = $data['observaciones'];
            $this->id_motivo = $data['id_motivo'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmtblPlantillaPlazas->select($this->id_plaza);
            $this->AdmcatMotivosBaja->select($this->id_motivo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_baja, a.curp, a.id_plaza, a.fecha_baja, a.num_oficio, a.fecha_oficio, a.observaciones, a.id_motivo,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status,
                  c.id_plaza, c.id_categoria, c.id_area, c.observaciones,
                  d.id_motivo, d.motivo, d.tipo
                FROM admtbl_bajas a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN admtbl_plantilla_plazas c ON a.id_plaza=c.id_plaza
                 LEFT JOIN admcat_motivos_baja d ON a.id_motivo=d.id_motivo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_baja' => $data['id_baja'],
                               'curp' => $data['curp'],
                               'id_plaza' => $data['id_plaza'],
                               'fecha_baja' => $data['fecha_baja'],
                               'num_oficio' => $data['num_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'observaciones' => $data['observaciones'],
                               'id_motivo' => $data['id_motivo'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admtbl_plantilla_plazas_id_categoria' => $data['id_categoria'],
                               'admtbl_plantilla_plazas_id_area' => $data['id_area'],
                               'admtbl_plantilla_plazas_observaciones' => $data['observaciones'],
                               'admcat_motivos_baja_motivo' => $data['motivo'],
                               'admcat_motivos_baja_tipo' => $data['tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }

    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.id_baja, a.curp, a.id_plaza, a.fecha_baja, a.num_oficio, a.id_motivo,
                  b.nombre, b.a_paterno, b.a_materno, b.genero,
                  d.categoria,
                  e.motivo, e.tipo
                FROM admtbl_bajas a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN admtbl_adscripcion c ON a.curp=c.curp
                 LEFT JOIN admcat_categorias d ON c.id_categoria=d.id_categoria
                 LEFT JOIN admcat_motivos_baja e ON a.id_motivo=e.id_motivo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_baja' => $data['id_baja'],
                               'curp' => $data['curp'],
                               'id_plaza' => $data['id_plaza'],
                               'fecha_baja' => $data['fecha_baja'],
                               'num_oficio' => $data['num_oficio'],
                               'id_motivo' => $data['id_motivo'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],                               
                               'genero' => $data['genero'],                               
                               'categoria' => $data['categoria'],                               
                               'motivo' => $data['motivo'],
                               'tipo' => $data['tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*)
                FROM admtbl_bajas a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN admtbl_adscripcion c ON a.curp=c.curp
                 LEFT JOIN admcat_categorias d ON c.id_categoria=d.id_categoria
                 LEFT JOIN admcat_motivos_baja e ON a.id_motivo=e.id_motivo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_bajas(id_baja, curp, id_plaza, fecha_baja, num_oficio, fecha_oficio, observaciones, id_motivo)
                VALUES(:id_baja, :curp, :id_plaza, :fecha_baja, :num_oficio, :fecha_oficio, :observaciones, :id_motivo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_baja" => $this->id_baja, ":curp" => $this->curp, ":id_plaza" => $this->id_plaza, ":fecha_baja" => $this->fecha_baja, ":num_oficio" => $this->num_oficio, ":fecha_oficio" => $this->fecha_oficio, ":observaciones" => $this->observaciones, ":id_motivo" => $this->id_motivo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_bajas
                   SET curp=:curp, id_plaza=:id_plaza, fecha_baja=:fecha_baja, num_oficio=:num_oficio, fecha_oficio=:fecha_oficio, observaciones=:observaciones, id_motivo=:id_motivo
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_baja" => $this->id_baja, ":curp" => $this->curp, ":id_plaza" => $this->id_plaza, ":fecha_baja" => $this->fecha_baja, ":num_oficio" => $this->num_oficio, ":fecha_oficio" => $this->fecha_oficio, ":observaciones" => $this->observaciones, ":id_motivo" => $this->id_motivo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>