<?php
require('fpdf.php');

class PDF extends FPDF
{
   //------------------ Variables miembro para la libreria MC_TABLE -----------------//
   var $widths;
   var $aligns;
   //--------------------------------------------------------------------------------//
   //------------- Variables miembro para la impresión de pie de pagina -------------//
   var $xInFooter = false;
   var $xPageNo = false;
   var $txt_footer_l;
   var $txt_footer_c;
   var $txt_footer_r;
   //--------------------------------------------------------------------------------//
   var $NewPageGroup;   // variable indicating whether a new group was requested
   var $PageGroups;     // variable containing the number of pages of the groups
   var $CurrPageGroup;  // variable containing the alias of the current page group
   //------------------------- Sobrecarga de Header y Footer ------------------------//
   function Header()
   {
      
   }
   
   function Footer()
   {
      $this->SetFont("Arial", "", 10);
      if( $this->xInFooter ){         
         $this->SetXY(10,-10);
         if( $this->txt_footer_l != '' )
            $this->Cell(0, 5, $this->txt_footer_l, "T", 0,"L");
         /*
         else
            $this->Cell(0, 5, date('d-m-Y'), "T", 0,"L");
         */
         
         $this->SetXY(10,-10);
         if( $this->txt_footer_c != '' )
            $this->Cell(0, 5, $this->txt_footer_c, "T", 0,"C");
               
         $this->SetXY(10,-10);   
         if( $this->txt_footer_r != '' )
            $this->Cell(0, 5, $this->txt_footer_r, "T", 0,"R");
         else
            $this->Cell(0, 5, $this->GroupPageNo().' / '.$this->PageGroupAlias(), "T", 0,"R");
      }
      else if( $this->xPageNo ){
         $this->SetXY(200, -10);
         $this->Cell(15, 5, $this->GroupPageNo().' / '.$this->PageGroupAlias(), 0, 0, "R");
      }
   }
   //--------------------------------------------------------------------------------//   
   
   //------------------ Funciones miembro para la libreria MC_TABLE -----------------//
   function SetWidths($w)
   {
   	//Set the array of column widths
   	$this->widths=$w;
   }
   
   function SetAligns($a)
   {
   	//Set the array of column alignments
   	$this->aligns=$a;
   }
   
   function Row($data)
   {
   	//Calculate the height of the row
   	$nb=0;
   	for($i=0;$i<count($data);$i++)
   		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
      $h = ( $nb > 2 ) ? 5*$nb : 5; 
      //$h=5*$nb;
   	//Issue a page break first if needed
   	$this->CheckPageBreak($h);
       
   	//Draw the cells of the row
   	for($i=0;$i<count($data);$i++)
   	{
   		$w=$this->widths[$i];
   		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
   		//Save the current position
   		$x=$this->GetX();
   		$y=$this->GetY();
   		//Draw the border
   		$this->Rect($x,$y,$w,$h);
   		//Print the text         
         if( $nb > 2 )
            $this->MultiCell($w,5,$data[$i],0,$a);
         else
            $this->CellFitScale($w, 5, $data[$i], 0, 0, $a);
   		//Put the position to the right of the cell
   		$this->SetXY($x+$w,$y);
   	}
   	//Go to the next line
   	$this->Ln($h);
   }
   
   function CheckPageBreak($h)
   {
   	//If the height h would cause an overflow, add a new page immediately
   	if($this->GetY()+$h>$this->PageBreakTrigger)
   		$this->AddPage($this->CurOrientation);
   }
   
   function NbLines($w,$txt)
   {
   	//Computes the number of lines a MultiCell of width w will take
   	$cw=&$this->CurrentFont['cw'];
   	if($w==0)
   		$w=$this->w-$this->rMargin-$this->x;
   	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
   	$s=str_replace("\r",'',$txt);
   	$nb=strlen($s);
   	if($nb>0 and $s[$nb-1]=="\n")
   		$nb--;
   	$sep=-1;
   	$i=0;
   	$j=0;
   	$l=0;
   	$nl=1;
   	while($i<$nb)
   	{
   		$c=$s[$i];
   		if($c=="\n")
   		{
   			$i++;
   			$sep=-1;
   			$j=$i;
   			$l=0;
   			$nl++;
   			continue;
   		}
   		if($c==' ')
   			$sep=$i;
   		$l+=$cw[$c];
   		if($l>$wmax)
   		{
   			if($sep==-1)
   			{
   				if($i==$j)
   					$i++;
   			}
   			else
   				$i=$sep+1;
   			$sep=-1;
   			$j=$i;
   			$l=0;
   			$nl++;
   		}
   		else
   			$i++;
   	}
   	return $nl;
   }
   //--------------------------------------------------------------------------------//
   function subWrite($h, $txt, $link='', $subFontSize=12, $subOffset=0)
   {
       // resize font
       $subFontSizeold = $this->FontSizePt;
       $this->SetFontSize($subFontSize);
       
       // reposition y
       $subOffset = ((($subFontSize - $subFontSizeold) / $this->k) * 0.3) + ($subOffset / $this->k);
       $subX        = $this->x;
       $subY        = $this->y;
       $this->SetXY($subX, $subY - $subOffset);
   
       //Output text
       $this->Write($h, $txt, $link);
   
       // restore y position
       $subX        = $this->x;
       $subY        = $this->y;
       $this->SetXY($subX,  $subY + $subOffset);
   
       // restore font size
       $this->SetFontSize($subFontSizeold);
   }
   
   function Circle($x, $y, $r, $style='D')
    {
        $this->Ellipse($x,$y,$r,$r,$style);
    }
    
    function Ellipse($x, $y, $rx, $ry, $style='D')
    {
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $lx=4/3*(M_SQRT2-1)*$rx;
        $ly=4/3*(M_SQRT2-1)*$ry;
        $k=$this->k;
        $h=$this->h;
        $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x+$rx)*$k,($h-$y)*$k,
            ($x+$rx)*$k,($h-($y-$ly))*$k,
            ($x+$lx)*$k,($h-($y-$ry))*$k,
            $x*$k,($h-($y-$ry))*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x-$lx)*$k,($h-($y-$ry))*$k,
            ($x-$rx)*$k,($h-($y-$ly))*$k,
            ($x-$rx)*$k,($h-$y)*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x-$rx)*$k,($h-($y+$ly))*$k,
            ($x-$lx)*$k,($h-($y+$ry))*$k,
            $x*$k,($h-($y+$ry))*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
            ($x+$lx)*$k,($h-($y+$ry))*$k,
            ($x+$rx)*$k,($h-($y+$ly))*$k,
            ($x+$rx)*$k,($h-$y)*$k,
            $op));
    }
    
    //------------- Funciones para ajustar el texto dentro de una celda -------------//
    //Cell with horizontal scaling if text is too wide
    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);

        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $ratio = ($w-$this->cMargin*2)/$str_width;

        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }

        //Pass on to Cell method
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);

        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }

    //Cell with horizontal scaling only if necessary
    function CellFitScale($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,true,false);
    }

    //Cell with horizontal scaling always
    function CellFitScaleForce($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,true,true);
    }

    //Cell with character spacing only if necessary
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }

    //Cell with character spacing always
    function CellFitSpaceForce($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        //Same as calling CellFit directly
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,true);
    }

    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }
    //-------------------------------------------------------------------------------//
    // create a new page group; call this before calling AddPage()
    function StartPageGroup()
    {
        $this->NewPageGroup = true;
    }

    // current page in the group
    function GroupPageNo()
    {
        return $this->PageGroups[$this->CurrPageGroup];
    }

    // alias of the current page group -- will be replaced by the total number of pages in this group
    function PageGroupAlias()
    {
        return $this->CurrPageGroup;
    }

    function _beginpage($orientation, $format)
    {
        parent::_beginpage($orientation, $format);
        if($this->NewPageGroup)
        {
            // start a new group
            $n = sizeof($this->PageGroups)+1;
            $alias = "{np$n}";
            $this->PageGroups[$alias] = 1;
            $this->CurrPageGroup = $alias;
            $this->NewPageGroup = false;
        }
        elseif($this->CurrPageGroup)
            $this->PageGroups[$this->CurrPageGroup]++;
    }

    function _putpages()
    {
        $nb = $this->page;
        if (!empty($this->PageGroups))
        {
            // do page number replacement
            foreach ($this->PageGroups as $k => $v)
            {
                for ($n = 1; $n <= $nb; $n++)
                {
                    $this->pages[$n] = str_replace($k, $v, $this->pages[$n]);
                }
            }
        }
        parent::_putpages();
    }
    //-------------------------------------------------------------------------------//
    
    //-------------------------- Javascript en PDF ----------------------------------//
    var $javascript;
    var $n_js;

    function IncludeJS($script){
        $this->javascript=$script;
    }

    function _putjavascript(){
        $this->_newobj();
        $this->n_js=$this->n;
        $this->_out('<<');
        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
        $this->_out('>>');
        $this->_out('endobj');
        $this->_newobj();
        $this->_out('<<');
        $this->_out('/S /JavaScript');
        $this->_out('/JS '.$this->_textstring($this->javascript));
        $this->_out('>>');
        $this->_out('endobj');
    }

    function _putresources(){
        parent::_putresources();
        if (!empty($this->javascript)) {
            $this->_putjavascript();
        }
    }

    function _putcatalog(){
        parent::_putcatalog();
        if (isset($this->javascript)){
            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
        }
    }
    
    function AutoPrint($dialog=false){
        //Embed some JavaScript to show the print dialog or start printing immediately
        $param = ($dialog ? 'true' : 'false');
        $script= "print($param);";
        $this->IncludeJS($script);
    }
}
?>
