var xGrid;
$(document).ready(function(){
    //-------------------------------------------------------------------//
    //------------- Asignacion de funcionalidad a controles -------------//
    //-------------------------------------------------------------------//
    //--- Ajusta el tamaño del contenedor del grid...
    $('#dvGridConflictosSociales').css('height', ($(window).height() - 200) + 'px');
    //--- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridConflictosSociales').xGrid({
        xColSort: 1,
        xHeight: 50,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 15,
        xTypeSelect: 1,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xTypeDataAjax: 'json'
    });
    
    
});