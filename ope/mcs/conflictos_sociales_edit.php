<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo de Conflictos Sociales
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mcs_conflictos.class.php';
include 'includes/class/opecat_mcs_municipios.class.php';
include 'includes/class/opecat_mcs_tipos_conflictos.class.php';
include 'includes/class/opecat_mcs_organizaciones_grupos.class.php';
include 'includes/class/opecat_mcs_tipos_movimientos.class.php';
$objConflictos = new OpetblMcsConflictos();
$objMunicipios = new OpecatMcsMunicipios();
$objTiposConflictos = new OpecatMcsTiposConflictos();
$objGrupos = new OpecatMcsOrganizacionesGrupos();
$objTiposMovimientos = new OpecatMcsTiposMovimientos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Conflictos Sociales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<link type="text/css" href="ope/mcs/_css/mcs.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="includes/js/tinymce_4.0.20/js/tinymce/tinymce.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="ope/mcs/_js/conflictos_sociales.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('conflictos_sociales_rg');
    $_SESSION['xIdConflicto'] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION['xIdConflicto'];
    $objConflictos->select($_SESSION['xIdConflicto']);
?>

    <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 60px;" title="Guardar los datos del nuevo registro...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 60px;" title="Cancelar la modificaci�n del registro...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <form id="frmConflictosSociales" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Conflictos" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/add.png" class="icono"/>
                Conflictos Sociales [Modificaci�n]
            </span>
            
            <fieldset id="fsetopetbl_mcs_conflictos_sociales" class="fsetForm-Data" >
                <!--<legend>opetb_mcs_conflictos</legend>-->
                <table id="tbfrmopetbl_mcs_conflictos_sociales" class="tbForm-Data">                    
                    <tr>
                        <td class="descripcion"><label for="txtFechaConflicto">Fecha del Conflicto:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaConflicto" id="txtFechaConflicto" style="width: 100px;" value="<?php echo date("d/m/Y", strtotime($objConflictos->fecha_conflicto));?>" class="" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="txtHoraInicio">Hora Inicio:</label></td>
                        <td class="validation">
                            <input type="text" name="txtHoraInicio" id="txtHoraInicio"  style="text-align: center; width: 50px;" value="<?php echo $objConflictos->hora_inicio;?>" maxlength="" class="" />
                            (00 - 24 horas)
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="txtHoraFin">Hora Fin:</label></td>
                        <td class="validation">
                            <input type="text" name="txtHoraFin" id="txtHoraFin"  style="text-align: center; width: 50px;" value="<?php echo $objConflictos->hora_fin;?>" maxlength="" class="" />
                            (00 - 24 horas)
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="cbxIdRegion">Region:</label></td>
                        <td class="validation">
                            <select name="cbxIdRegion" id="cbxIdRegion" style="width: 250px;">
                            <?php
                            echo $objMunicipios->OpecatMcsRegiones->getRegiones($objConflictos->id_region);
                            ?>              
                            </select>
                            <span class="pRequerido">*</span>
                            <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('ope/mcs/_ajx/ajx_obt_municipios.php');?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="cbxIdMunicipio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxIdMunicipio" id="cbxIdMunicipio" style="width: 250px;">
                            <?php
                            echo $objMunicipios->getMunicipios($objConflictos->id_municipio, 0, $objConflictos->id_region);
                            ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="txtLocalidad">Localidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtLocalidad" id="txtLocalidad" style="width: 300px;" value="<?php echo $objConflictos->localidad;?>" maxlength="80" class="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="cbxIdTipoConflicto">Tipo de Conflicto:</label></td>
                        <td class="validation">
                            <select name="cbxIdTipoConflicto" id="cbxIdTipoConflicto" style="width: 300px;">
                                <?php 
                                echo $objTiposConflictos->getTiposConflictos($objConflictos->id_tipo_conflicto);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="cbxIdTipoMovimiento">Tipo de Movimiento:</label></td>
                        <td class="validation">
                            <select name="cbxIdTipoMovimiento" id="cbxIdTipoMovimiento" style="width: 300px;">
                                <?php
                                echo $objTiposMovimientos->getTiposMovimientos($objConflictos->id_tipo_movimiento);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="cbxIdOrganizacionGpo">Organizacion/Grupo:</label></td>
                        <td class="validation">
                            <select name="cbxIdOrganizacionGpo" id="cbxIdOrganizacionGpo" style="width: 300px;">
                                <?php 
                                echo $objGrupos->getOrganizacionesGrupos($objConflictos->id_organizacion_gpo);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="txtDirigentes">Dirigentes:</label></td>
                        <td class="validation">
                            <!-- <textarea name="textarea" id="txtAreaDirigentes" cols="45" rows="2"></textarea> -->
                            <input type="text" name="txtDirigentes" id="txtDirigentes" style="width: 300px;" value="<?php echo $objConflictos->dirigentes;?>" maxlength="" class="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="descripcion"><label for="txtNumeroInvolucrados">Numero de Involucrados:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumeroInvolucrados" id="txtNumeroInvolucrados"  style="width: 60px;" value="<?php echo $objConflictos->numero_involucrados;?>" maxlength="" class="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="descripcion"><label for="txtAreaHechosHtml">Descripcion de Hechos:</label></td>
                    </tr>
                    <tr>
                        <td  colspan="2"  class="validation">
                            <textarea name="txtAreaHechosHtml" id="txtAreaHechosHtml" style="width: 100%;" cols="50" rows="10"><?php echo $objConflictos->hechos_text;?></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <!-- Fin del Contenido del Formulario -->
            <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
        </div>
        <input type="hidden" name="hdnTypeOper" id="hdnTypeOper" value="2" />
    </form>
<?php
  //-----------------------------------------------------------------//
  //-- Bloque de cerrado de la plantilla...
  //-----------------------------------------------------------------//
  $plantilla->paginaFin();
?>
