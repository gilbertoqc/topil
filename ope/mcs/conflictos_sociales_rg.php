<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mcs_conflictos.class.php';
$objConflictos = new OpetblMcsConflictos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Conflictos Sociales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                //'scripts' => array('<script type="text/javascript" src="adm/_js/rhumanos/personal/datosrg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Procesamiento');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <script type="text/javascript">
        $(document).ready(function(){
            if ($('#hdnError').val() == '') {
                //location.href = "index.php?" + $('#hdnUrl').val();
                //-- Mensaje...        
                var msj = $(getHTMLMensaje('Felicidades !!! el proceso se llev� a cabo correctamente.', 1));
                msj.dialog({
                    autoOpen: true,                        
                    minWidth: 400,
                    resizable: false,
                    modal: true,
                    buttons:{
                        'Aceptar': function(){
                            location.href = "index.php?" + $('#hdnUrl').val();
                        }
                    }
                });
            } else {
                //-- Error...
                var msj = $(getHTMLMensaje('Ohh, al parecer ha ocurrido un error !!!... Detalles:<br />' + $('#hdnError').val(), 3));
                msj.dialog({
                    autoOpen: true,                        
                    minWidth: 450,
                    resizable: false,
                    modal: true,
                    buttons:{
                        'Volver a intentarlo': function(){
                            location.href = "index.php?" + $('#hdnUrl').val();
                        },
                        'Cancelar el proceso': function(){
                            location.href = "index.php" + $('#hdnUrl').val();
                        }
                    }
                });
            }
        });
    </script>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $objConflictos->id_conflicto = ( $_POST["hdnTypeOper"] == 1 ) ? 0 : $_SESSION["xIdConflicto"];
    $objConflictos->fecha_conflicto = $objSys->convertirFecha($_POST["txtFechaConflicto"], 'yyyy-mm-dd');
    $objConflictos->hora_inicio = ( !empty($_POST["txtHoraInicio"]) ) ? $_POST["txtHoraInicio"] : null;
    $objConflictos->hora_fin = ( !empty($_POST["txtHoraFin"]) ) ? $_POST["txtHoraFin"] : null;
    $objConflictos->id_region = $_POST["cbxIdRegion"];
    $objConflictos->id_municipio = $_POST["cbxIdMunicipio"];
    $objConflictos->localidad = $_POST["txtLocalidad"];
    $objConflictos->id_tipo_conflicto = $_POST["cbxIdTipoConflicto"];
    $objConflictos->id_tipo_movimiento = $_POST["cbxIdTipoMovimiento"];
    $objConflictos->id_organizacion_gpo = $_POST["cbxIdOrganizacionGpo"];
    $objConflictos->dirigentes = $_POST["txtDirigentes"];
    $objConflictos->numero_involucrados = ( !empty($_POST["txtNumeroInvolucrados"]) ) ? $_POST["txtNumeroInvolucrados"] : null;
    $objConflictos->hechos_html = $_POST["txtAreaHechosHtml"];
    $texto = strip_tags($_POST["txtAreaHechosHtml"]);
    //$texto = str_replace(chr(13), '', $texto);
    $objConflictos->hechos_text = $texto;
    $objConflictos->fecha_agregado = date("Y-m-d");
    
    $result = ( $_POST["hdnTypeOper"] == 1 ) ? $objConflictos->insert() : $objConflictos->update();
    //-------------------------------------------------------------------//
    if ($result) {
        if( $_POST["hdnTypeOper"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;
            $mod = "conflictos_sociales_edit";
        } else {
            $oper = "Edt";
            $id_reg = $_SESSION["xIdConflicto"];
            $mod = "conflictos_sociales";
        }        
        $objSys->registroLog($objUsr->idUsr, "opetbl_mcs_conflictos", $id_reg, $oper);
        $error = '';
    } else {        
        $error = (!empty($objConflictos->msjError)) ? $objConflictos->msjError : 'Error al guardar los datos del Conflicto Social';        
    }
    
    $url_mod = (empty($error)) ? "" : "&mod=" . $objSys->encrypt($mod); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . $url_mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>