$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tamaño del contenedor del grid... ***/
    $('#dvGridAntecedentesCriminales').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridAntecedentesCriminales').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xTypeDataAjax: 'json',
    });
    
    //Aplica la propiedad de ToolTip a todos los elementos que contengan la propiedad title
    $(document).tooltip({
          tooltipClass: "toolTipDetails",
          content: function () {
              return $(this).prop('title');
          },
          show:{
              effect: "clip",
              delay: 250
          },
          position: { my: "left center", at: "right center" }
      }).addClass("efecto-over");
      
      
    $("#txtFechaAntecedente").datepicker({
       yearRange:'c-1:c',
   });

   //$("#txtFechaAntecedente").focus();

   $("#txtFechaLiberacion").datepicker({
       yearRange: 'c-1:c',
   });



/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/
//$('#txtFechaAntecedente').blur(function(){ $(this).val(xTrim($(this).val().toUpperCase() )) });
$('#txtDelito').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });//
//$('#txtFechaLiberacion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para el formulario **************/
/********************************************************************/

/***** Aplicación de las reglas de validación de los campos del formulario *****/
  $('#frmAntecedentesCriminales').validate({
      rules:{
      /***** INICIO de Reglas de validacion para el formulario de xxxxx *****/
        txtFechaAntecedente:{
            required:true,
        },
        txtDelito:{
            required:true,
        },
        txtFechaLiberacion:{
            required:true,
        },
      /***** FIN de Reglas de validacion para el formulario de xxxxx *****/
     },
     messages:{
     /***** INICIO de Mensajes de validacion para el formulario de xxxxx *****/
        txtFechaAntecedente:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo sólo acepta digitos"></span>',
         },
        txtDelito:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtFechaLiberacion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo sólo acepta digitos"></span>',
        },

     /***** FIN de Mensajes de validacion para el formulario de xxxxx *****/
     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
  /***** Fin de validacion *****/
  });



/********************************************************************/
/***** Control del formulario para agregar/modifiar             *****/
/********************************************************************/
 $('#dvFormAntecedentesCriminales').dialog({
        show: {
            effect: "fade",duration: 500
        },
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        buttons: {
            'Guardar': function(){
               GuardarAntecedentesCriminales();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
        open: function(){
            resetFormulario();
            if ($('#txtIdPerfilCriminal').val() == 0) {
                $(this).dialog('option', 'title', 'SISP :: Agregar Antecedentes Crimnales');
            }else{
                $(this).dialog('option', 'title', 'SISP :: Antecedentes Crimnales');
                //datosReferencia();
            }
        }
    });

    $('#btnAgregar').click(function(){
        //$('#hdnIdRef').val('0');
        //$('#hdnTypeOper').val('1');
        $('#dvFormAntecedentesCriminales').dialog('open');
    });



/***** Fin del document.ready *****/
});



/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acción del botón Guardar
function GuardarAntecedentesCriminales(){
    var valida = $('#frmAntecedentesCriminales').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los cambios realizados en Antecedentes Criminales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $('#frmAntecedentesCriminales').submit();
                },
                'Cancelar': function(){
                    $(this).dialog('close');
                }
            }
        });
    }
}

/*Reinicia Formulario de la captura de datos criminales*/
function resetFormulario(){
    $('#txtIdPerfilCriminal').val(0);
    $('#txtFechaAntecedente').val('');
    $('#txtDelito').val('');
    $('#txtFechaLiberacion').val('');

    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}