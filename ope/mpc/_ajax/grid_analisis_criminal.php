<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mpc_analisis_criminal.class.php';
    $objSys = new System();
    $objDataGridAnalisisCriminal = new OpetblMpcAnalisisCriminal();

    //--------------------- Recepci�n de par?metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = 'ac.id_analisis_criminal LIKE ? '
                    . 'OR pc.id_perfil_criminal LIKE ? '
                    . 'OR pc.nombre LIKE ? '
                    . 'OR pc.apellido_paterno LIKE ? '
                    . 'OR pc.apellido_paterno LIKE ? '
                    . 'OR ac.fecha_analisis LIKE ? '
                    . 'OR ac.analisis_criminal LIKE ? '
                    . 'OR ac.id_usuario LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%'
                            );
   }
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'ac.id_analisis_criminal ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'pc.id_perfil_criminal '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'pc.apellido_paterno ' . $_GET['typeSort']  . ', pc.apellido_materno ' . $_GET['typeSort']  . ', pc.nombre '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'ac.fecha_analisis ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'ac.analisis_criminal ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'ac.id_usuario ' . $_GET['typeSort'];
    }
    // Paginaci?n...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridAnalisisCriminal->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $dato["id_analisis_criminal"]; //$objSys->encrypt($dato["id_persona"]);
                $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_analisis_criminal"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 2%;">';
                    // $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 5%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_analisis_criminal"].'</strong></td>';
                //$html .= '<td style="text-align: center; width: 10%;">' . $dato["id_perfil_criminal"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["nombre"] .' '. $dato["apellido_paterno"] .' '. $dato["apellido_materno"] . '</td>';
                $html .= '<td style="text-align: center; width: 7%;">' . $dato["fecha_analisis"] . '</td>';
                $html .= '<td style="text-align: left; width: 43%;">' . $dato["analisis_criminal"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; text-transform:uppercase;">' . $dato["id_usuario"].'</td>';

                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("---") . "&id=" . $id_crypt;
                $url_del = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("---") . "&id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Editar Analisis ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_del . '" class="lnkBtnOpcionGrid" title="Imprimir Analisis ..." ><img src="' . PATH_IMAGES . 'icons/pdf24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; font-size:18px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de Analisis Criminal ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>