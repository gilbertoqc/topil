<?php
/**
 * @author Markino
 * @copyright 2014
 * MODULO PERFIL CRIMINAL
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Antecedentes Criminales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link   type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mpc/_js/antecedentes_criminales.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();


//se reciben parametros
$id_perfil = $objSys->decrypt($_GET["id"]);
//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('perfil_panel'). "&id=" . $objSys->encrypt($id_perfil);
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('antecedentes_criminales_save');
?>

    <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                     <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Agregar Nuevos Datos Criminales...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                    </a>
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar al Panel del Perfil Criminal...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
     </div>


  
<div id="dvForm-Antecedentes" class="dvForm-Data">
    <span class="dvForm-Data-pTitle">
        <img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" class="icono"/>
         Antecedentes Criminales
    </span>            
  
    <!--****** INICIO TABLA DE DATOS  *****-->
    <div id="dvGridAntecedentesCriminales" style="border: none; margin: auto auto; margin-top: 4px; width: auto;">
        <div class="xGrid-dvHeader gradient">
          <table class="xGrid-tbSearch">
              <tr>
                  <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
              </tr>
          </table>
          <table class="xGrid-tbCols">
              <tr>
                  <th style="width: 2%; text-align: center;">&nbsp;</th>
                  <th style="width: 5%;" class="xGrid-tbCols-ColSortable">ID</th>
                  <th style="width: 15%;" class="xGrid-thNo-Class">NOMBRE</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">FECHA</th>
                  <th style="width: 40%;" class="xGrid-tbCols-ColSortable">DELITO</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">LIBERACION</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable"></th>
              </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
          <table class="xGrid-tbBody">
              <?php
    
              ?>
          </table>
        </div>
    </div>
</div>
<!--****** FIN TABLA DE DATOS  ******-->
  
  
    <!-- Formulario -->
<div id="dvFormAntecedentesCriminales" title="SISP :: ">
<form id="frmAntecedentesCriminales" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
    <fieldset id="fsetopetbl_mpc_antecedentes_criminales" class="fsetForm-Data">
         <!--</a><legend>opetbl_mpc_antecedentes_criminales</legend>-->
         <table id="tbfrmopetbl_mpc_antecedentes_criminales" class="tbForm-Data">
           <!--<tr>
               <td><label for="txtIdAntecedente">IdAntecedente:</label></td>
               <td class="validation">
                  <input type="text" name="txtIdAntecedente" id="txtIdAntecedente" value="" maxlength="" class="" />
                  <span class="pRequerido">*</span>
                </td>
        
           </tr>-->
           <tr>
               <td class="descripcion"><label for="txtIdPerfilCriminal">Id Perfil Criminal:</label></td>
               <td class="validation">
                  <input type="text" name="txtIdPerfilCriminal" id="txtIdPerfilCriminal" value="0" maxlength="" class="" disabled="disabled" readonly="true" />
                  <span class="pRequerido">*</span>
                </td>
        
           </tr>
           <tr>
               <td class="descripcion"><label for="txtFechaAntecedente">Fecha de Antecedente:</label></td>
               <td class="validation">
                  <input type="text" name="txtFechaAntecedente" id="txtFechaAntecedente" value="" maxlength="10"  class="" />
                  <span class="pRequerido">*</span>
                </td>
        
           </tr>
           <tr>
               <td class="descripcion"><label for="txtDelito">Delito Cometido:</label></td>
               <td class="validation">
                  <input type="text" name="txtDelito" id="txtDelito" value="" maxlength="100" class=""style="width: 400px;" />
                  <span class="pRequerido">*</span>
                </td>
        
           </tr>
           <tr>
               <td class="descripcion"><label for="txtFechaLiberacion">Fecha de Liberacion:</label></td>
               <td class="validation">
                  <input type="text" name="txtFechaLiberacion" id="txtFechaLiberacion" value=""  maxlength="10" class="" />
                </td>
        
           </tr>
         </table>
    </fieldset>
    <!-- Fin de Formulario -->
    <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
    <input type="hidden" name ="hdnUrlDatos" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mpc/_ajax/grid_antecedentes_criminales.php');?>" />
 </form>
</div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>