<?php
/**
 * @author Markinho
 * @copyright 2014
 */
 
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opecat_mpc_estados_civiles.class.php';
$objEdoCivil = new OpecatMpcEstadosCiviles();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Incidentes Agregar',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="includes/js/jquery.tree-select.js"></script>',
                                   '<script type="text/javascript" src="rhumanos/_js/personal/persona.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">&nbsp;</td>
            </tr>
        </table>
    </div>
    <?php
        
    $url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidente_add');
    ?>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 1000px;">
            <span class="dvForm-Data-pTitle"><img src="includes/css/imgs/icons/pin_blue24.png" style="border: none; margin-right: 7px; vertical-align: middle;" />
                Registro de Incidentes
            </span>                                
            <div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
             <h1>Incidentes Add</h1> 
                          
           
           </div>
                   
        <input type="hidden" name="dtTypeOper" value="1" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
    <!-- Botones de opci�n... -->
    <div id="dvBarraOpciones" class="dvBar-Float">
        <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 90px;" title="Validar y continuar...">
            <img src="includes/css/imgs/icons/back241.png" alt="" style="border: none;" /><br />Anterior
        </a>                
        <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Validar y continuar...">
            <img src="includes/css/imgs/icons/next24.png" alt="" style="border: none;" /><br />Siguiente
        </a>
        <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 110px;" title="Guardar los datos del nuevo elemento...">
            <img src="includes/css/imgs/icons/ok24.png" alt="" style="border: none;" /><br />Guardar
        </a>
        <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
            <img src="includes/css/imgs/icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
        </a>
    </div>   
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>