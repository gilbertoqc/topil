
$(document).ready(function(){
    var popup;
    var n=1;
    var options = {
        zoom: 14
        , center: new google.maps.LatLng(17.538563, -99.500202)
        , mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('mapaEstadisticas'), options);

    var place = new Array();
    place['Ignacio Ram?rez 72-13'] = new google.maps.LatLng(17.545745, -99.498851);
    place['5 de May. 38, San Mateo'] = new google.maps.LatLng(17.555401, -99.501940);
    place['Meceda, El Mirador'] = new google.maps.LatLng(17.555074, -99.512326);
    place[''] = new google.maps.LatLng(17.534502, -99.5029279);
    place['Disparos con Arma de Fuego'] = new google.maps.LatLng(17.557314, -99.508399);
    place['Accidente de Transito con Heridos'] = new google.maps.LatLng(17.554798, -99.506264);
    place['Privacion Ilegal de la libertad'] = new google.maps.LatLng(17.551688, -99.508496);
    place['Robo a Comercio'] = new google.maps.LatLng(17.542410, -99.503850);
    place['Asalto a Transunte'] = new google.maps.LatLng(17.538052, -99.504387);
    place['Incendio de Casa Habitacion'] = new google.maps.LatLng(17.545151, -99.510802);



    for(var i in place){
        var marker = new google.maps.Marker({
            position: place[i]
            , map: map
            , title: i
            , icon: 'http://google-maps-icons.googlecode.com/files/darkblue0' + n++ + '.png'
        });

        //icon: 'http://gmaps-samples.googlecode.com/svn/trunk/markers/red/marker' + n++ + '.png'

        google.maps.event.addListener(marker, 'click', function(){
            if(!popup){
                popup = new google.maps.InfoWindow();
            }
            var note = '<strong>Lugar:</strong> ' + this.title;
            popup.setContent(note);
            popup.open(map, this);
        })
    }


});


/*
Ignacio Ram?rez 72-13, Cuauht?moc Sur, 22000 Chilpancingo de Los Bravo, GRO, M?xico
17.545745085858293
-99.49885101321411


5 de May. 38, San Mateo, Chilpancingo de Los Bravo, GRO, M?xico
17.555401659391002
-99.50194091799926


Meceda, El Mirador, Chilpancingo de Los Bravo, GRO, M?xico
17.555074326344478
-99.51232643130493


Nogal, Olinal?, Chilpancingo de Los Bravo, GRO, M?xico
17.534502303780776
-99.50292797091674


A. Elizundia de Calvo 30B, Santa Cruz Norte, Chilpancingo de Los Bravo, GRO, M?xico
17.55731450005105
-99.50839967730712

*/

/*
//Creamos un punto en el Mapa
//M?s marcadores en http://code.google.com/p/google-maps-icons/wiki/AllUpdates.
window.onload = function(){
    var options = {
        zoom: 9
        , center: new google.maps.LatLng(18.2, -66.5)
        , mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map'), options);

    new google.maps.Marker({
        position: map.getCenter()
        , map: map
        , title: 'Pulsa aqu?'
        , icon: 'http://gmaps-samples.googlecode.com/svn/trunk/markers/green/blank.png'
        , cursor: 'default'
        , draggable: true
    });
};
*/
