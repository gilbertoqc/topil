$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/

 $('#divTabGraficos').tabs(); 

/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/




/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/




/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/



/***** Fin del document.ready *****/
});