<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');    
    //-- Bloque de inclusi�n de las clases...
    $path = '../../../';    
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_personal.class.php';
    $objUsr  = new Usuario(); 
    $objSys  = new System();
    $objIncidPol = new OpetblMidIncidentesPersonal();
   
    // datos de la persona
    $objIncidPol->id_folio_incidente    = $_SESSION["xIdIncidente"];
    $objIncidPol->curp                  = $_POST["id"];    
        
    if( $_POST["tp"] == 1 ){
        $result = $objIncidPol->insert();
        $oper = "Ins";
    } else if( $_POST["tp"] == 2 ) {
        $result = $objIncidPol->delete($_SESSION["xIdIncidente"], $_POST["id"]);
        $oper = "Dlt";
    }
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, "opetbl_mid_incidentes_personal", $_POST["id"], $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objIncidPol->msjError;
    }
          
    echo json_encode($ajx_datos);
}else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>