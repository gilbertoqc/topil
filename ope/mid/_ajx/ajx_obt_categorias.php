<?php
/**
 * Complemento del llamado ajax para listar las marcas dentro de un combobox. 
 * @param int id_clase, recibido por el m�todo GET.
 * @param int id_modelo, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/opecat_mid_generalidades.class.php';
    $objCat = new OpecatMidGeneralidades();        
    
    if ( $_POST["id_tipo"] != 0 ){
        $datos .= $objCat->getCat_mid_Generalidades( $_POST["id_tipo"], $_POST["id_categoria"] );
    }
    if (empty($objCat->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objCat->msjError;        
    }   
         
        
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>