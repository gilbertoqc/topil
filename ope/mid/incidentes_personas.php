<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva Incidentes->Personas
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mid_incidentes_personas.class.php';

$objInc = new OpetblMidIncidentesPersonas;

//se reciben parametros
//id_folio_incidente
$_SESSION["xIdIncidente"] = ( isset($_GET["id_folio_incidente"]) ) ? $objSys->decrypt($_GET["id_folio_incidente"]) : $_SESSION["xIdIncidente"];

//-----------------------------------------------------------------//
//-- Bloque de definici?n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidentes->Personas',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_personas.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('incidentes_panel') . "&id_folio_incidente=" . $objSys->encrypt( $_SESSION["xIdIncidente"] );
  //$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_personas_rg');

?>

<div id="dvTool-Bar" class="dvTool-Bar">
    <table>
        <tr>
            <td class="tdNombreModulo">
                <?php $plantilla->mostrarNombreModulo();?>
            </td>
            <td class="tdBotonesAccion">
                <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo ...">
                    <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                </a>
            </td>
        </tr>
    </table>
</div>



<div id="dvForm-Personas" class="dvForm-Data" style="margin-top: 10px; width: 1200px;">
    <span class="dvForm-Data-pTitle">
        <img src="<?php echo PATH_IMAGES;?>icons/people24.png" class="icono"/>
         Personas
    </span>

    <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
        <ul>
            <li><a href="#tabs-1" style="font-size: 11pt; min-width: 150px; text-align: center;">Personas involucradas</a></li>
            <li><a href="#tabs-2" style="font-size: 11pt; min-width: 150px; text-align: center;">Personal que atendi�</a></li>
        </ul>
        <!-- Tab de Personas involucradas -->
        <div id="tabs-1" style="padding-bottom: 50px;">
            <!-- Boton de Agregar mediante una caja de Dialogo -->
            <div class="dvBar-Normal" style="margin: auto auto; margin-top: 7px; padding: 3px 1px 3px 1px; text-align: right; width: 100%;">
                <a href="#" id="btnAddPer" class="Tool-Bar-Btn gradient" style="color: #ffffff; width: 80px;" title="Agregar persona involucrada ...">
                    <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                </a>
            </div>

            <!--Grid -->
            <div id="dvGridPer" style="border: none; height: 250px; margin: auto auto; margin-top: 10px; width: auto;">
                <div class="xGrid-dvHeader gradient">
                    <table class="xGrid-tbSearch">
                        <tr>
                            <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGridPer']) ) echo $_SESSION['xBuscarGridPer'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                        </tr>
                    </table>
                    <table class="xGrid-tbCols">
                        <tr>
                            <th style="width: 4%; text-align: center;">&nbsp;</th>
                            <th style="width: 15%;" class="xGrid-tbCols-ColSortable">ROL</th>
                            <th style="width: 15%;" class="xGrid-tbCols-ColSortable">CAUSA</th>
                            <th style="width: 22%;" class="xGrid-tbCols-ColSortable">NOMBRE</th>
                            <th style="width: 6%;" class="xGrid-tbCols-ColSortable">SEXO</th>
                            <th style="width: 6%;" class="xGrid-tbCols-ColSortable">EDAD</th>
                            <th style="width: 23%;" class="xGrid-thNo-Class">DOMICILIO</th>
                            <th style="width: 10%;" class="xGrid-thNo-Class">&nbsp;</th>
                        </tr>
                    </table>
                </div>
                <div class="xGrid-dvBody"  style="min-height: 100px;">

                </div>
            </div>
        </div>
        <!-- Tab de personal que atendi� -->
        <div id="tabs-2" style="padding-bottom: 50px;">
            <!-- Boton de Agregar mediante una caja de Dialogo -->
            <div class="dvBar-Normal" style="margin: auto auto; margin-top: 7px; padding: 3px 1px 3px 1px; text-align: right; width: 100%;">
              <a href="#" id="btnAddPol" class="Tool-Bar-Btn gradient" style="color: #ffffff; width: 80px;" title="Agregar personal operativo...">
                  <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
              </a>
            </div>

            <!--Grid -->
            <div id="dvGridPol" style="border: none; height: 250px; margin: auto auto; margin-top: 10px; width: 1150px;">
                <div class="xGrid-dvHeader gradient">
                    <table class="xGrid-tbSearch">
                        <tr>
                            <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGridPol']) ) echo $_SESSION['xBuscarGridPol'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                        </tr>
                    </table>
                    <table class="xGrid-tbCols">
                        <tr>
                            <th style="width: 4%; text-align: center;">&nbsp;</th>
                            <th style="width: 25%;" class="xGrid-tbCols-ColSortable">NOMBRE</th>
                            <th style="width: 19%;" class="xGrid-tbCols-ColSortable">CUIP</th>
                            <th style="width: 18%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                            <th style="width: 12%;" class="xGrid-tbCols-ColSortable">NIVEL MANDO</th>
                            <th style="width: 12%;" class="xGrid-tbCols-ColSortable">STATUS</th>
                            <th style="width: 10%;" class="xGrid-thNo-Class">&nbsp;</th>
                        </tr>
                    </table>
                </div>
                <div class="xGrid-dvBody"  style="min-height: 100px;">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de alta o modificaci�n de datos para Personas Involucradas -->
<div id="dvFormPersona" title="SISP :: ">
    <form id="frmPersona" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <input type="hidden" name="hdnIdPersona" id="hdnIdPersona" value="0" />
        <!-- Contenido del Formulario -->
        <fieldset class="fsetForm-Data">
            <table class="tbForm-Data">
                <tr>
                    <td class="descripcion"><label for="cbxIdRolPersona">Persona Rol:</label></td>
                    <td class="validation">
                        <select name="cbxIdRolPersona" id="cbxIdRolPersona" style="max-width: 350px;">
                            <?php echo $objInc->OpecatMidPersonasRolesCausas->OpecatMidPersonasRoles->getCat_mid_Personas_Roles(); ?>
                        </select>
                        <span class="pRequerido">*</span>
                        <input type="hidden" id="hdnUrlCausas" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_obt_causas.php'); ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="cbxIdCausaPersona">Persona Causa:</label></td>
                    <td class="validation">
                        <select name="cbxIdCausaPersona" id="cbxIdCausaPersona" style="max-width: 350px;">
                         <option value="0">-- SELECCIONE UN ROL --</option>
                        </select>
                        <span class="pRequerido">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="txtNombre">Nombre:</label></td>
                    <td class="validation">
                        <input type="text" name="txtNombre" id="txtNombre"  style="width: 250px;" value="" maxlength="35" class="" />
                        <span class="pRequerido">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="txtApellidoPaterno">Apellido Paterno:</label></td>
                    <td class="validation">
                        <input type="text" name="txtApellidoPaterno" id="txtApellidoPaterno"  style="width: 250px;" value="" maxlength="30" class="" />
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="txtApellidoMaterno">Apellido Materno:</label></td>
                    <td class="validation">
                        <input type="text" name="txtApellidoMaterno" id="txtApellidoMaterno"  style="width: 250px;" value="" maxlength="30" class="" />
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="txtEdad">Edad:</label></td>
                    <td class="validation">
                        <input type="text" name="txtEdad" id="txtEdad"  style="width: 100px;" value="" maxlength="" class="" />
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="rbnSexo">Sexo:</label></td>
                    <td class="validation">
                        <div class="dvRadioGroup">
                            <label class="label-Radio Masculino" style="margin-right: 10px;">
                                <input type="radio" name="rbnSexo" id="rbnSexo1" value="1" checked="true" />Masculino</label>
                            <label class="label-Radio Femenino">
                                <input type="radio" name="rbnSexo" id="rbnSexo2" value="2" />Femenino</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="descripcion"><label for="txtAreaDomicilio">Domicilio:</label></td>
                    <td class="validation">
                        <textarea name="txtAreaDomicilio" id="txtAreaDomicilio"  style="height: 55px; width: 400px;"></textarea>
                    </td>
                </tr>
            </table>
        </fieldset>
        <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" style="vertical-align: middle;" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
        <input type="hidden" id="hdnTypeOper" name="hdnTypeOper" value="1" />
    </form>
</div>
<input type="hidden" id="hdnUrlDatosPer" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_personas.php');?>" />
<input type="hidden" id="hdnUrlSavePer" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_reg_personas.php');?>" />
<input type="hidden" id="hdnUrlGetPer" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_obt_datos_persona.php');?>" />
<input type="hidden" id="hdnUrlDltPer" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_eliminar_persona.php');?>" />

<!-- B�squeda de personal operativo -->
<div id="dvFormBuscaPersonal" title="SISP :: B�squeda de personal operativo">
    <div id="dvGrid-BuscaPol" style="border: none; margin-top: 5px; width: 930px;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" value="" style="width: 220px;" /></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>
                    <th style="width: 5%; text-align: center;">&nbsp;</th>
                    <th style="width: 30%;" class="xGrid-tbCols-ColSortable">NOMBRE</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">CUIP</th>
                    <th style="width: 22%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">NIVEL MANDO</th>
                    <th style="width: 8%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 250px;">

        </div>
    </div>
</div>
<input type="hidden" id="hdnUrlDatosPol" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_personal.php');?>" />
<input type="hidden" id="hdnUrlGridPol" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_busca_pol.php');?>" />
<input type="hidden" id="hdnUrlProcesPol" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_proces_incid_pol.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>